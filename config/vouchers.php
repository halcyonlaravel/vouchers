<?php

declare(strict_types=1);

return [

    'tables' => [
        'vouchers' => 'voucher_vouchers',
        'requirements' => 'voucher_voucher_requirements',
        'computations' => 'voucher_voucher_computations',
        'computation_models' => 'voucher_computation_models',
        'user_voucher' => 'voucher_user_voucher',
        'limits' => 'voucher_voucher_limits',
        'incompatibles' => 'voucher_incompatibles',
        'users' => 'users',
    ],

    'models' => [
        'computation' => HalcyonLaravelBoilerplate\Vouchers\Models\Computation::class,
        'computation_model' => HalcyonLaravelBoilerplate\Vouchers\Models\ComputationModel::class,
        'incompatible' => HalcyonLaravelBoilerplate\Vouchers\Models\Incompatible::class,
        'limit' => HalcyonLaravelBoilerplate\Vouchers\Models\Limit::class,
        'requirement' => HalcyonLaravelBoilerplate\Vouchers\Models\Requirement::class,
        'voucher' => HalcyonLaravelBoilerplate\Vouchers\Models\Voucher::class,
        'user' => 'App\Models\Auth\User',
    ],

    'mapping' => [
        'requirements' => [

        ],
        'computations' => [

        ],
    ],

];
