# Changelog

All notable changes to `vouchers` will be documented in this file

## 0.11.0 - 2023-10-09

- Use lloricode/check-digit:^v3.0.1

## 0.11.0-beta.2 - 2023-10-09

- Use lloricode/check-digit:^v3

## 0.11.0-beta.1 - 2023-03-24

- Use https://packagist.org/

## 0.11.0-alpha.8 - 2023-03-23

- Add exception class for voucher already used.

## 0.11.0-alpha.7 - 2023-03-03

- Use actions for all functionality.
- Rename Object helper to *Data
- Try to fix guest check limit

## 0.11.0-alpha.6 - 2023-02-01

- Add ability to throw exception in `useCode`.
- Install rector, then run for php 8.0

## 0.11.0-alpha.5 - 2023-02-01

- Add support for laravel 10

## 0.11.0-alpha.4 - 2023-02-01

- Add sharedLock on get limit count in guest

## 0.11.0-alpha.3 - 2023-02-01

- Revert (Add cache lock on checking limit.[0.11.0-alpha])

## 0.11.0-alpha.2 - 2023-02-01

- Add sharedLock on get limit count

## 0.11.0-alpha - 2023-02-01

- Add cache lock on checking limit.

## 0.10.1 - 2023-01-25

- Add and fixes prevent lazy loading on testing

## 0.10.0 - 2023-01-24

- Stable phpstan and larastan
- Add support laravel 9
- Install pint formatter

## 0.9.2 - 2023-01-20

- Add support moneyphp/money ^4.1

## 0.9.1 - 2022-10-21

- Add $with in Voucher::usedOrders() and Voucher::fetchBatch()

## 0.9.0 - 2022-07-11

- Fix for requiredMorph
- Change order to orderable, and user to userable in limit model
- Add config environment for ray() in testing

## 0.8.3 - 2021-10-27

- Add dev roave/security-advisories
- Change PHPunit to Pestphp
- Add dev spatie/laravel-ray
- Add dev phpstan/phpstan
- Fix code base by phpstan
- Add static code for ide helper

## 0.8.2 - 2021-06-22

- Implement spatie/laravel-package-tools

## 0.8.1 - 2021-06-17

- Clean Up
- Replace parent class exception to invalidArgument of all exception class

## 0.8.0 - 2021-06-08

- Add updated_at in limit model

## 0.7.3 - 2021-06-08

- Enhance setUnUsed

## 0.7.2 - 2021-04-23

- Add support for PHP8

## 0.7.1 - 2021-04-05

- Allow custom message in LimitUsageVoucherRule

## 0.7.0 - 2020-12-07

- Remove support below laravel 8 to use `castUsing` in cast class

## 0.6.0 - 2020-12-07

- Add Incompatible (working in progress)
- Fix event trigger when override model implementations
- Fix remove class in voucher
- Touch model voucher when update computation or requirement or eligibility or incompatible
- Fix remove relationship on update voucher
- Add type and typeLabel in base computation
- Add contract for limit, order and user
- Add usedOrders
- Remove special characters in voucher code (store/update)

## 0.5.4 - 2020-11-16

- Add Rule for voucher date

## 0.5.3 - 2020-11-03

- typo

## 0.5.2 - 2020-11-03

- Add Validation in voucher helper start vs end date

## 0.5.1 - 2020-11-03

- Add UniqueByBatchRule
- Add function to Manager to call validation rules

## 0.5.0 - 2020-11-03

- Move namespace all rules 

## 0.4.3 - 2020-11-02

- Fix `limit_usage` in update

## 0.4.2 - 2020-10-30

- Add skip users in migration down
- Require "composer-runtime-api": "^2.0" in --dev
- Add getComputationModelModelsArray() in Voucher model
- Add fetchBatch in voucher model
- Use observer class

## 0.4.1 - 2020-10-20

- Add validation when destroy voucher

## 0.4.0 - 2020-10-20

- Add ability to update requirements and computation
- Add specific user on create voucher
- Rename $requirement_helpers, $computation_helper to $requirements, $computation

## 0.3.1 - 2020-10-15

- Add abstract class for voucher helper class

## 0.3.0 - 2020-10-15

- Set Multiple model for computation
- Update config for tables, to same in actual model names
- Rename all attributes/fields to `$class` for `requirements` and `computation`
- Add implementation config in all models
- Rename Vouchers to VoucherManager
- Add `isBulk()` and `getPrefix()` in voucher model
- Add ability to update voucher via bulk or single
- Add voucher `getUsedCount(bool $bulk = false): int`
- Rename models inside formula class
- Add validateComputations in facade implementation/concrete
- Add delete function
- Rename `limit` to `limit_usage`
    
## 0.2.0 - 2020-10-06

- Add a table for requirements and computations
- Add down in migration
- Rename models

## 0.1.0 - 2020-10-02

- Save voucher_id in the limit, to ensure cannot reuse in future when actual current code destroyed

## 0.0.3 - 2020-09-21

- Set nullable requirements in migration
- Typo in migration comments
- Add docs VoucherHelper->limit
- Set default null in VoucherHelper->requirements
- Add branch alias for composer

## 0.0.2 - 2020-09-11

- Typo from COSTUMERS to CUSTOMERS

## 0.0.1 - 2020-09-09

- initial release
