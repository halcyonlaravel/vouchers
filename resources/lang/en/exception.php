<?php

declare(strict_types=1);

return [
    'unique' => 'Unique code `:code` exceed in mask `:mask` at `:times` times.',
    'cannot_destroy' => 'Cannot delete promo, already in use.',
    'already_used' => 'Voucher already used.',
];
