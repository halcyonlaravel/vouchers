<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

return [
    'requirements' => [
        'test_basic_rule_formula' => 'Test Basic Rule',
    ],
    'computations' => [
        'test_basic_computation_formula' => 'Test Basic Computation',
    ],
    'eligibilities' => [
        Voucher::ELIGIBILITY_EVERYONE => 'Everyone',
        Voucher::ELIGIBILITY_REGISTER_CUSTOMERS => 'Registered Costumers',
        Voucher::ELIGIBILITY_SPECIFIC_CUSTOMERS => 'Specific Costumers',
    ],
];
