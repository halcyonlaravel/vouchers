<?php

declare(strict_types=1);

return [
    'limit' => 'Promo code not found.',
    'eligibility' => 'Promo code not found.',
    'class' => 'Promo code not found.',
    'unique_by_batch' => 'The :attribute already in use.',
];
