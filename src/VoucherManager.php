<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers;

use HalcyonLaravelBoilerplate\Vouchers\Actions\CreateVoucherAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\DeleteVoucherAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\ForSelectAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\GetVoucherBuilderAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\UnUseVoucherAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\UpdateVoucherAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\UseVoucherAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\VoucherCanBeUseAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\VoucherValidateComputationAction;
use HalcyonLaravelBoilerplate\Vouchers\Contracts\OrderableLimitVoucherContract;
use HalcyonLaravelBoilerplate\Vouchers\Contracts\UserableLimitVoucherContract;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\CodeGeneratorData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;
use HalcyonLaravelBoilerplate\Vouchers\Http\Rules\LimitUsageVoucherRule;
use HalcyonLaravelBoilerplate\Vouchers\Http\Rules\StartEndDateRule;
use HalcyonLaravelBoilerplate\Vouchers\Http\Rules\UniqueByBatchRule;
use HalcyonLaravelBoilerplate\Vouchers\Http\Rules\ValidEligibilityVoucherRule;
use HalcyonLaravelBoilerplate\Vouchers\Http\Rules\ValidForVoucherRule;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Throwable;

final class VoucherManager
{
    public static function query(): EloquentBuilder
    {
        return app(GetVoucherBuilderAction::class)->execute();
    }

    public static function all(): Collection
    {
        return app(GetVoucherBuilderAction::class)->execute()->get();
    }

    /**
     * @return \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher|array<int, \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher>
     *
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException|Throwable
     */
    public static function create(
        VoucherData $voucherHelper,
        CodeGeneratorData $generatorHelper = null,
        int $amount = 1
    ): Voucher|array {
        return app(CreateVoucherAction::class)->execute(...func_get_args());
    }

    /**
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException
     * @throws Throwable
     */
    public static function update(VoucherUpdateData $voucherUpdateHelper): void
    {
        app(UpdateVoucherAction::class)->execute(...func_get_args());
    }

    /** @throws Throwable */
    public static function delete(string|array $valueSearch, bool $includeBatch = false, string $where = 'code'): void
    {
        app(DeleteVoucherAction::class)->execute(...func_get_args());
    }

    public static function useCode(
        UserableLimitVoucherContract|Model $user, // TODO: UserableLimitVoucherContract&Model on PHP 8.1
        OrderableLimitVoucherContract|Model $order, // TODO: OrderableLimitVoucherContract&Model on PHP 8.1
        string $code,
        bool $throwExceptionOnFailed = false
    ): void {
        app(UseVoucherAction::class)->execute(...func_get_args());
    }

    public static function unUseCode(Model $user, Model $order, string $code): void
    {
        app(UnUseVoucherAction::class)->execute(...func_get_args());
    }

    public static function checkLimit(string $code, Model $user = null): bool
    {
        return app(VoucherCanBeUseAction::class)->execute(...func_get_args());
    }

    /** @return array<string, string> */
    public static function forSelect(string $type): array
    {
        return app(ForSelectAction::class)->execute(...func_get_args());
    }

    /**
     * @param  Voucher[]  $vouchers
     *
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException
     */
    public static function validateComputations(array $vouchers): array
    {
        return app(VoucherValidateComputationAction::class)->execute(...func_get_args());
    }

    public static function limitUsageVoucherRule(?Model $user): LimitUsageVoucherRule
    {
        return new LimitUsageVoucherRule($user);
    }

    public static function uniqueByBatchRule(string $column = 'title'): UniqueByBatchRule
    {
        return new UniqueByBatchRule($column);
    }

    public static function validEligibilityVoucherRule(): ValidEligibilityVoucherRule
    {
        return new ValidEligibilityVoucherRule();
    }

    /** @param  class-string  $type */
    public static function validForVoucherRule(string $type): ValidForVoucherRule
    {
        return new ValidForVoucherRule($type);
    }

    public static function startEndDateRule(string $endFieldName): StartEndDateRule
    {
        return new StartEndDateRule($endFieldName);
    }
}
