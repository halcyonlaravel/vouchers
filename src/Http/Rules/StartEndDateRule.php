<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Http\Rules;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Contracts\Validation\Rule;

class StartEndDateRule implements Rule
{
    private ?string $message = null;

    public function __construct(public string $endFieldName)
    {
    }

    private static function parseDatetime(Carbon|string $datetime): bool|Carbon
    {
        try {
            $return = is_string($datetime)
                ? now()->parse($datetime)
                : $datetime;
        } catch (InvalidFormatException) {
            return false;
        }

        return $return;
    }

    public function passes($attribute, $value): bool
    {
        $endDate = null;

        if (
            filled($requestData = request()->input($this->endFieldName)) &&
            ($endDate = self::parseDatetime($requestData)) === false
        ) {
            $this->message = sprintf('The `%s` is invalid.', $this->endFieldName);

            return false;
        }

        if (($startDate = self::parseDatetime($value)) === false) {
            return false;
        }

        if (filled($endDate)) {
            /** @phpstan-ignore-next-line */
            return $endDate->greaterThan($startDate);
        }

        return true;
    }

    public function message(): array|string
    {
        return $this->message ?? 'The :attribute is invalid.';
    }
}
