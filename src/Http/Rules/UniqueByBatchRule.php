<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Http\Rules;

use HalcyonLaravelBoilerplate\Vouchers\Models;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Contracts\Validation\Rule;

class UniqueByBatchRule implements Rule
{
    private ?Voucher $voucher = null;

    public function __construct(private string $column = 'title')
    {
    }

    public function ignore(Voucher $voucher): self
    {
        $this->voucher = $voucher;

        return $this;
    }

    public function passes($attribute, $value): bool
    {
        $MODEL = Models::voucher();

        $query = $MODEL->query()->where($this->column, $value)->groupBy('batch');

        if (! is_null($this->voucher)) {
            $query->where('batch', '!=', $this->voucher->batch);
        }

        return $query->count($this->column) == 0;
    }

    public function message()
    {
        return trans('vouchers::validation.unique_by_batch');
    }
}
