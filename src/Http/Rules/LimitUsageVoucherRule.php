<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Http\Rules;

use HalcyonLaravelBoilerplate\Vouchers\Actions\VoucherCanBeUseAction;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Model;

class LimitUsageVoucherRule implements Rule
{
    private ?string $message = null;

    public function __construct(private ?Model $user)
    {
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function passes($attribute, $value): bool
    {
        return app(VoucherCanBeUseAction::class)->execute($value, $this->user);
    }

    public function message()
    {
        return $this->message ?? trans('vouchers::validation.limit');
    }
}
