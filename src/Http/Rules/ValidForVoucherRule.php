<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Http\Rules;

use HalcyonLaravelBoilerplate\Vouchers\Actions\ForSelectAction;
use Illuminate\Contracts\Validation\Rule;

class ValidForVoucherRule implements Rule
{
    private array $classes;

    public function __construct(string $type)
    {
        $this->classes = app(ForSelectAction::class)->execute($type);
    }

    public function passes($attribute, $value): bool
    {
        return array_key_exists($value, $this->classes);
    }

    public function message()
    {
        return trans('vouchers::validation.class');
    }
}
