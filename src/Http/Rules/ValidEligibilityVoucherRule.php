<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Http\Rules;

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Contracts\Validation\Rule;

class ValidEligibilityVoucherRule implements Rule
{
    public function passes($attribute, $value): bool
    {
        return in_array($value, Voucher::ELIGIBILITIES);
    }

    public function message()
    {
        return trans('vouchers::validation.eligibility');
    }
}
