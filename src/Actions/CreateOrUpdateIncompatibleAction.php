<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\BaseVoucherData;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class CreateOrUpdateIncompatibleAction
{
    public function execute(BaseVoucherData $baseVoucherData): void
    {
        //        if (blank($baseVoucherData->incompatibleHelper)) {
        //            // TODO: remove all
        //            return;
        //        }

        //        if ($baseVoucherData instanceof VoucherUpdateData && ! $baseVoucherData->isIncompatibleDirty()) {
        //            return;
        //        }

        //        foreach ($baseVoucherData->incompatibleHelper->computations ?: [] as $computation) {
        //        }
        //        foreach ($baseVoucherData->incompatibleHelper->vouchers ?: [] as $voucher) {
        //        }
        //        foreach ($baseVoucherData->incompatibleHelper->voucher_batches ?: [] as $batch) {
        //        }
    }
}
