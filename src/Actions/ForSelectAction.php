<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Helper;

final class ForSelectAction
{
    /** @return array<string, string> */
    public function execute(string $type): array
    {
        $classes = Helper::getClasses($type);

        /** @var array<string, string> $return */
        $return = [];

        foreach ($classes as $class) {
            $return[(string) $class::name()] = (string) $class::label();
        }

        return $return;
    }
}
