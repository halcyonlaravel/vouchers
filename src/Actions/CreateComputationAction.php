<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class CreateComputationAction
{
    public function execute(Voucher $voucher, VoucherData $voucherData): void
    {
        /** @var \HalcyonLaravelBoilerplate\Vouchers\Models\Computation $computationModelCreated */
        $computationModelCreated = $voucher->computation()
            ->create(
                [
                    'class' => $voucherData->computation->class,
                    'value' => $voucherData->computation->value,
                ]
            );

        foreach ($voucherData->computation->models ?: [] as $model) {
            $computationModelCreated->computationModels()
                ->create(
                    [
                        'model_type' => $model->getMorphClass(),
                        'model_id' => $model->getKey(),
                    ]
                );
        }
    }
}
