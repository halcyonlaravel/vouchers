<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\OrderableLimitVoucherContract;
use HalcyonLaravelBoilerplate\Vouchers\Contracts\UserableLimitVoucherContract;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\VoucherCannotUsedException;
use HalcyonLaravelBoilerplate\Vouchers\Models;
use Illuminate\Database\Eloquent\Model;

final class UseVoucherAction
{
    public function __construct(private VoucherCanBeUseAction $voucherCanBeUseAction)
    {
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\VoucherCannotUsedException */
    public function execute(
        UserableLimitVoucherContract $user, // TODO: UserableLimitVoucherContract&Model on PHP 8.1
        OrderableLimitVoucherContract $order, // TODO: OrderableLimitVoucherContract&Model on PHP 8.1
        string $code,
        bool $throwExceptionOnFailed = false
    ): void {
        if (! $order instanceof Model) {
            ValidationException::throw('order must extend '.Model::class);
        } elseif (! $user instanceof Model) {
            ValidationException::throw('user must extend '.Model::class);
        } elseif (! $this->voucherCanBeUseAction->execute($code)) {
            if ($throwExceptionOnFailed) {
                throw VoucherCannotUsedException::throw();
            } else {
                return;
            }
        }

        Models::limit()::create(
            [
                /** @phpstan-ignore-next-line */
                'voucher_id' => Models::voucher()->whereCode($code)->first()->getKey(),
                'userable_type' => $user->getMorphClass(),
                'userable_id' => $user->getKey(),
                'orderable_type' => $order->getMorphClass(),
                'orderable_id' => $order->getKey(),
                'code' => $code,
            ]
        );
    }
}
