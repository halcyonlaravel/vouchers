<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class GetVoucherPrefixAction
{
    public function execute(Voucher $voucher): string
    {
        if (! $voucher->isBulk()) {
            return $voucher->code;
        }

        $codes = Models::voucher()::where('batch', $voucher->batch)
            ->limit(2)
            ->select('code')
            ->pluck('code');

        $prefix = '';
        foreach (str_split($codes[0]) as $key => $char) {
            if ($char != str_split($codes[1])[$key]) {
                break;
            }
            $prefix .= $char;
        }

        return $prefix;
    }
}
