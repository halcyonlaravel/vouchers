<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

final class DeleteVoucherAction
{
    public function execute(
        string|array $valueSearch,
        bool $includeBatch = false,
        string $where = 'code'
    ): void {
        $valid = [Models::voucher()->getRouteKeyName(), Models::voucher()->getKeyName()];
        if (blank($valueSearch)) {
            return;
        }

        if (! in_array($where, $valid, true)) {
            ValidationException::throw(
                __METHOD__.": third argument invalid given `$where`, valid is [".
                implode(', ', $valid).'].'
            );
        }

        $voucher = Models::voucher()->whereIn($where, Arr::wrap($valueSearch));

        if (blank($voucher->first())) {
            abort(404);
        }

        /**
         * event will not trigger when issuing mass update or delete
         * https://laravel.com/docs/8.x/eloquent#events
         *
         * @param  \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher  $voucher
         * @return bool|null
         */
        $delete = fn (Voucher $voucher) => $voucher->delete();

        DB::transaction(
            function () use ($voucher, $includeBatch, $delete) {
                if ($includeBatch) {
                    /** @phpstan-ignore-next-line */
                    Models::voucher()->whereBatch($voucher->first()->batch)->each($delete);

                    return;
                }

                $voucher->each($delete);
            }
        );
    }
}
