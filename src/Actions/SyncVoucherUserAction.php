<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\BaseVoucherData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Foundation\Auth\User;

final class SyncVoucherUserAction
{
    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    public function execute(Voucher $voucher, BaseVoucherData $baseVoucherData): void
    {
        if (blank($baseVoucherData->users)) {
            $voucher->users()->sync([]);

            return;
        }

        if ($baseVoucherData instanceof VoucherUpdateData && ! $baseVoucherData->isUsersDirty()) {
            return;
        }

        $ids = [];
        /** @phpstan-ignore-next-line */
        foreach ($baseVoucherData->users as $user) {
            if (! is_subclass_of($user, User::class)) {
                ValidationException::throw('User models must subclass of '.User::class);
            }
            $ids[] = $user->getKey();
        }

        $voucher->users()->sync($ids);
    }
}
