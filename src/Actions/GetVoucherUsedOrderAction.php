<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class GetVoucherUsedOrderAction
{
    public function execute(
        Voucher $voucher,
        string $status = Voucher::STATUS_USED,
        array $with = []
    ): array {
        $return = [];

        foreach ($voucher->fetchBatch($status, ['*'], $with) as $voucher) {
            /** @var Voucher $voucher */
            $deletedCount = 0;
            foreach ($voucher->limits as $limit) {
                $orderTitle = $limit->orderable?->voucherLimitTitle();
                if ($orderTitle === null) {
                    $deletedCount++;
                } else {
                    $return[$voucher->code][] = $orderTitle;
                }
            }
            if ($deletedCount !== 0) {
                $return[$voucher->code][] = "DELETED ($deletedCount)";
            }
        }

        return $return;
    }
}
