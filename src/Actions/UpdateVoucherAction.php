<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;
use HalcyonLaravelBoilerplate\Vouchers\Models;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Support\Facades\DB;

final class UpdateVoucherAction
{
    public function __construct(
        private SyncVoucherUserAction $syncVoucherUserAction,
        private CreateOrUpdateIncompatibleAction $createOrUpdateIncompatibleAction,
        private CreateOrUpdateRequirementsAction $createOrUpdateRequirementsAction,
    ) {
    }

    public function execute(VoucherUpdateData $voucherUpdateData): void
    {
        $voucherUpdateData->validate();

        $updateData = [
            'title' => $voucherUpdateData->title,
            'description' => $voucherUpdateData->description,

            'eligibility' => $voucherUpdateData->eligibility,

            'limit_usage' => $voucherUpdateData->limit,
            'limit_per_user' => $voucherUpdateData->limit_per_user,

            'valid_start_at' => $voucherUpdateData->valid_start_at,
            'valid_end_at' => $voucherUpdateData->valid_end_at,

            'remarks' => $voucherUpdateData->remarks,
        ];

        if (! $voucherUpdateData->isBulk()) {
            $updateData['code'] = $voucherUpdateData->code;
        }

        DB::transaction(
            function () use ($voucherUpdateData, $updateData) {
                Models::voucher()
                    ->with([
                        'computation.computationModels',
                        'requirements',
                    ])
                    ->where('batch', $voucherUpdateData->batch)
                    ->each(
                        function (Voucher $voucher) use ($voucherUpdateData, $updateData) {
                            $voucher->update($updateData);

                            if ($voucherUpdateData->isComputationDirty()) {
                                $voucher->computation->update(
                                    [
                                        'class' => $voucherUpdateData->computation->class,
                                        'value' => $voucherUpdateData->computation->value,
                                    ]
                                );

                                $voucher->computation->computationModels->each->delete();
                                foreach ($voucherUpdateData->computation->models ?? [] as $model) {
                                    $voucher->computation->computationModels()->create(
                                        [
                                            'model_type' => $model->getMorphClass(),
                                            'model_id' => $model->getKey(),
                                        ]
                                    );
                                }
                            }

                            $this->createOrUpdateRequirementsAction->execute($voucher, $voucherUpdateData);

                            $this->syncVoucherUserAction->execute($voucher, $voucherUpdateData);

                            $this->createOrUpdateIncompatibleAction->execute($voucherUpdateData);

                            if (
                                $voucherUpdateData->isComputationDirty() ||
                                $voucherUpdateData->isRequirementsDirty() ||
                                $voucherUpdateData->isUsersDirty() ||
                                $voucherUpdateData->isIncompatibleDirty()
                            ) {
                                $voucher->touch();
                            }
                        }
                    );
            }
        );
    }
}
