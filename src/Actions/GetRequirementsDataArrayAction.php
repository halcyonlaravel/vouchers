<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class GetRequirementsDataArrayAction
{
    public function execute(Voucher $voucher): array
    {
        $voucher->loadMissing([
            'requirements.model',
        ]);

        $return = [];

        foreach ($voucher->requirements as $requirement) {
            $class = $requirement->class;

            $model = null;
            $modelUrl = null;
            if ($requirement->model) {
                $model = $requirement->model;
                /** @phpstan-ignore-next-line */
                $modelUrl = $model->getRouteKey();
            }

            $return[] = [
                'label' => $class::label(),
                'name' => $class::name(),
                'value' => $requirement->value,
                'model_url' => $modelUrl,
                'model' => $model,
            ];
        }

        return $return;
    }
}
