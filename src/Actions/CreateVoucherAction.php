<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\CodeGeneratorData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\VoucherGenerator;
use Illuminate\Support\Facades\DB;
use Lloricode\CheckDigit\Generator;

final class CreateVoucherAction
{
    public function __construct(
        private CreateComputationAction $createComputationAction,
        private SyncVoucherUserAction $syncVoucherUserAction,
        private CreateOrUpdateIncompatibleAction $createOrUpdateIncompatibleAction,
        private CreateOrUpdateRequirementsAction $createOrUpdateRequirementsAction,
    ) {
    }

    public function execute(
        VoucherData $voucherData,
        CodeGeneratorData $codeGeneratorData = null,
        int $amount = 1
    ): Voucher|array {
        if ($amount < 1) {
            ValidationException::throw("Minimum amount generate must not below 1, $amount given.");
        }

        $voucherData->validate();
        $voucherData->generator_helper = $codeGeneratorData;
        $vouchers = [];

        $batchNumber = $voucherData->batch ?: self::batchNumber();

        DB::transaction(
            function () use ($amount, $codeGeneratorData, $voucherData, &$vouchers, $batchNumber) {
                foreach (self::generate($amount, $codeGeneratorData) as $code) {
                    /** @var \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher $voucherModelCreated */
                    $voucherModelCreated = Models::voucher()->create(
                        [
                            'batch' => $batchNumber,

                            'title' => $voucherData->title,
                            'code' => $amount == 1 ? ($voucherData->code ?? $code) : $code,
                            'description' => $voucherData->description,

                            'eligibility' => $voucherData->eligibility,

                            'limit_usage' => $voucherData->limit,
                            'limit_per_user' => $voucherData->limit_per_user,

                            'valid_start_at' => $voucherData->valid_start_at,
                            'valid_end_at' => $voucherData->valid_end_at,

                            'remarks' => $voucherData->remarks,
                        ]
                    );

                    $this->createComputationAction->execute($voucherModelCreated, $voucherData);

                    $this->createOrUpdateRequirementsAction->execute($voucherModelCreated, $voucherData);

                    $this->syncVoucherUserAction->execute($voucherModelCreated, $voucherData);

                    $this->createOrUpdateIncompatibleAction->execute($voucherData);

                    $vouchers[] = $voucherModelCreated;
                }
            }
        );

        return $amount === 1 ? $vouchers[0] : $vouchers;
    }

    private static function batchNumber(): int
    {
        /** @var Voucher $model */
        $model = Models::voucher()
            ->distinct()
            ->latest('batch')
            ->select('batch')
            ->first();

        if (blank($model)) {
            return 1;
        }

        return $model->batch + 1;
    }

    /**
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException
     * @throws \Lloricode\CheckDigit\Exceptions\ValidationException
     */
    private static function generate(int $amount = 1, CodeGeneratorData $generatorHelper = null): array
    {
        $codes = [];

        for ($i = 1; $i <= $amount; $i++) {
            if (! in_array($code = self::getUniqueVoucher($amount, $generatorHelper), $codes)) {
                $codes[] = $code;
            } else {
                $i--;
            }
        }

        return $codes;
    }

    /**
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException
     * @throws \Lloricode\CheckDigit\Exceptions\ValidationException
     */
    private static function getUniqueVoucher(int $amount, CodeGeneratorData $generatorHelper = null): string
    {
        $voucherModel = Models::voucher();
        $code = self::generateCode($generatorHelper);

        $maxTry = $voucherModel->count() + $amount;
        $try = 0;

        while ($voucherModel->whereCode($code)->count() > 0) {
            $code = self::generateCode($generatorHelper);
            if (++$try === $maxTry) {
                ValidationException::throw(
                    trans(
                        'vouchers::exception.unique',
                        [
                            'code' => $code,
                            'mask' => $generatorHelper?->mask ?? (new CodeGeneratorData())->mask,
                            'times' => $try,
                        ]
                    )
                );
            }
        }

        return $code;
    }

    /** @throws \Lloricode\CheckDigit\Exceptions\ValidationException */
    private static function generateCode(CodeGeneratorData $generatorHelper = null): string
    {
        $generatorHelper ??= new CodeGeneratorData();

        if ($generatorHelper->checkDigit) {
            $generatorHelper->characters = '1234567890';
            $generatorHelper->separator = '';

            $maskLength = $generatorHelper->checkDigitFormat->length() -
                strlen((string) $generatorHelper->prefix) - 1;

            $generatorHelper->mask = str_pad('', $maskLength, '*');
        }

        $generator = new VoucherGenerator($generatorHelper->characters, $generatorHelper->mask);
        $generator->setPrefix($generatorHelper->prefix);
        $generator->setSuffix($generatorHelper->suffix);
        $generator->setSeparator($generatorHelper->separator);

        $generated = $generator->generate();

        if ($generatorHelper->checkDigit) {
            return Generator::new($generated, $generatorHelper->checkDigitFormat)->getValue();
        }

        return $generated;
    }
}
