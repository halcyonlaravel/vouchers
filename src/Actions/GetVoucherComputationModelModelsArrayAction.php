<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class GetVoucherComputationModelModelsArrayAction
{
    public function execute(Voucher $voucher): array
    {
        $return = [];

        foreach ($voucher->computation->computationModels as $computationModel) {
            $model = $computationModel->model;

            $return[] = [
                'model_url' => $model->getRouteKey(),
                'model' => $model,
            ];
        }

        return $return;
    }
}
