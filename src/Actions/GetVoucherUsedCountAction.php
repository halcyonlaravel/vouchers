<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class GetVoucherUsedCountAction
{
    public function execute(Voucher $voucher, bool $bulk = false): int
    {
        if ($bulk) {
            $count = 0;

            foreach (Models::voucher()::whereBatch($voucher->batch)->get('code') as $voucher) {
                /** @var Voucher $voucher */
                $count += Models::limit()::getUsedCountByVoucherCode($voucher->code);
            }

            return $count;
        }

        return Models::limit()::getUsedCountByVoucherCode($voucher->code);
    }
}
