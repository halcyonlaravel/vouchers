<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\BaseVoucherData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class CreateOrUpdateRequirementsAction
{
    public function execute(Voucher $voucher, BaseVoucherData $baseVoucherData): void
    {
        if (blank($baseVoucherData->requirements)) {
            $voucher->requirements->each->delete();

            return;
        }

        if ($baseVoucherData instanceof VoucherUpdateData && ! $baseVoucherData->isRequirementsDirty()) {
            return;
        }

        if ($baseVoucherData instanceof VoucherUpdateData) {
            $voucher->requirements->each->delete();
        }

        /** @phpstan-ignore-next-line */
        foreach ($baseVoucherData->requirements as $requirementHelper) {
            $data = [
                'class' => $requirementHelper->class,
            ];

            if (filled($requirementHelper->value)) {
                $data['value'] = $requirementHelper->value;
            } else {
                if ($requirementHelper->model === null) {
                    abort(500, '$requirementHelper->model is null');
                }

                $data['model_type'] = $requirementHelper->model->getMorphClass();
                $data['model_id'] = $requirementHelper->model->getKey();
            }

            $voucher->requirements()
                ->create($data);
        }
    }
}
