<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models;
use Illuminate\Database\Eloquent\Model;

final class UnUseVoucherAction
{
    public function execute(Model $user, Model $order, string $code): void
    {
        $voucher = Models::voucher()::whereCode($code)->first();

        if ($voucher === null) {
            return;
        }

        Models::limit()::used()->where(
            [
                'voucher_id' => $voucher->getKey(),
                'userable_type' => $user->getMorphClass(),
                'userable_id' => $user->getKey(),
                'orderable_type' => $order->getMorphClass(),
                'orderable_id' => $order->getKey(),
                'code' => $code,
                'used' => true,
            ]
        )->update(
            [
                'used' => false,
            ]
        );
    }
}
