<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

final class GetVoucherBatchCollectionAction
{
    /** @return \Illuminate\Database\Eloquent\Collection|\HalcyonLaravelBoilerplate\Vouchers\Models\Voucher[] */
    public function execute(
        Models\Voucher $voucher,
        string $status = Models\Voucher::STATUS_ALL,
        array $columns = ['code'],
        array $with = []
    ): Collection {
        $result = Models::voucher()::whereBatch($voucher->batch)
            ->with(['limits.orderable', ...$with]);

        switch ($status) {
            case Models::voucher()::STATUS_ALL:
                // nothing to do
                break;
            case Models::voucher()::STATUS_USED:
                $result = $result->whereHas(
                    'limits',
                    function (Builder $query) {
                        $query->where('used', true);
                    }
                );

                break;
            case Models::voucher()::STATUS_UNUSED:
                $result = $result
                    ->where(
                        function (Builder $query) {
                            $query->whereDoesntHave(
                                'limits',
                                function (Builder $query) {
                                    $query->where('used', true);
                                }
                            )->orDoesntHave(
                                'limits'
                            );
                        }
                    );

                break;
            default:
                abort(500, __METHOD__.": Invalid status `$status`.");
        }

        return $result->get($columns);
    }
}
