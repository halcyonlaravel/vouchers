<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Database\Eloquent\Model;

final class VoucherCanBeUseAction
{
    public function execute(string $code, Model $user = null): bool
    {
        /** @var Voucher|null $voucher */
        $voucher = Models::voucher()->whereCode($code)->first();

        if ($voucher === null) {
            return false;
        }

        $usedCount = Models::limit()::whereCode($code)->used()->sharedLock()->count();

        $noneUser = $voucher->limit_usage === 0 || $usedCount < $voucher->limit_usage;
        unset($usedCount);

        if ($user === null) {
            return $noneUser;
        }

        $userClass = config('vouchers.models.user');

        if ($user instanceof $userClass) {
            $usedPerUserCount = Models::limit()::whereCode($code)
                ->used()
                ->whereMorphedTo('userable', $user)
                ->sharedLock()
                ->count();

            $withUser = $voucher->limit_per_user === 0 || $usedPerUserCount < $voucher->limit_per_user;

            return $withUser && $noneUser;
        }

        return $noneUser;
    }
}
