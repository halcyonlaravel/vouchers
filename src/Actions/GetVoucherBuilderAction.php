<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\DB;

final class GetVoucherBuilderAction
{
    public function execute(): Builder
    {
        $id = Models::voucher()->getKeyName();

        return Models::voucher()
            ->query()
            ->whereIn(
                $id,
                fn (QueryBuilder $query) => $query->select(DB::raw("min($id)"))
                    ->from(Models::voucher()->getTable())
                    ->groupBy('batch')
            );
    }
}
