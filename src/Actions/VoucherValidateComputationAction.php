<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Actions;

use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Helper;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class VoucherValidateComputationAction
{
    /**
     * @param  Voucher[]  $vouchers
     *
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException
     */
    public function execute(array $vouchers): array
    {
        /** @var Voucher[] $vouchers */
        /** @var \HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\ComputationContract[] $computationClasses */
        $computationClasses = [];

        array_map(
            function ($voucher) use (&$computationClasses) {
                if (! $voucher instanceof Voucher) {
                    ValidationException::throw(
                        self::class.'::validateComputations argument must array of '.config('vouchers.models.voucher')
                    );
                }
                $computationClasses[] = $voucher->computation->class;
            },
            $vouchers
        );

        $errors = [];

        foreach ($computationClasses as $key => $computationClass) {
            foreach ($computationClasses as $keyCompare => $computationClassCompare) {
                if ($key == $keyCompare) {
                    continue;
                }

                if (! $computationClass->rules()->canMultipleUse) {
                    if ($computationClass::type() == $computationClassCompare::type() ||
                        $computationClass::name() == $computationClassCompare::name()) {
                        $errors[] = sprintf('`%s` can only use once.', $computationClass::type());
                    }
                }

                if (filled($incompatibles = $computationClass->rules()->incompatibleWith)) {
                    foreach ($incompatibles as $incompatible) {
                        if (
                            $incompatible::name() == $computationClassCompare::name()) {
                            $errors[] = sprintf(
                                '`%s` incompatible with `%s`.',
                                $computationClass->voucher()->title,
                                $computationClassCompare->voucher()->title,
                            );
                        }
                    }
                }
            }
        }

        $search = [];
        $replace = [];
        foreach (Helper::getComputationClasses() as $c) {
            $search[] = $c::type();
            $replace[] = $c::typeLabel();
        }

        return str_replace($search, $replace, array_unique($errors));
    }
}
