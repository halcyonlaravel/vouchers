<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers;

use Illuminate\Support\Str;

/**
 * https://github.com/beyondcode/laravel-vouchers/blob/1.2.0/src/VoucherGenerator.php
 */
final class VoucherGenerator
{
    protected ?string $prefix = null;

    protected ?string $suffix = null;

    protected string $separator = '-';

    public function __construct(
        protected string $characters = 'ABCDEFGHJKLMNOPQRSTUVWXYZ234567890',
        protected string $mask = '****-****'
    ) {
    }

    public function setSeparator(string $separator): void
    {
        $this->separator = $separator;
    }

    public function generate(): string
    {
        $length = substr_count($this->mask, '*');

        $code = $this->getPrefix();
        $mask = $this->mask;
        $characters = collect(str_split($this->characters));

        for ($i = 0; $i < $length; $i++) {
            $mask = Str::replaceFirst('*', (string) $characters->random(1)->first(), $mask);
        }

        $code .= $mask;
        $code .= $this->getSuffix();

        return $code;
    }

    protected function getPrefix(): string
    {
        return $this->prefix !== null ? $this->prefix.$this->separator : '';
    }

    public function setPrefix(?string $prefix): void
    {
        $this->prefix = $prefix;
    }

    protected function getSuffix(): string
    {
        return $this->suffix !== null ? $this->separator.$this->suffix : '';
    }

    public function setSuffix(?string $suffix): void
    {
        $this->suffix = $suffix;
    }
}
