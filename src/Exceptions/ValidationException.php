<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Exceptions;

use InvalidArgumentException;

final class ValidationException extends InvalidArgumentException
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function throw(string $message): self
    {
        throw new self($message);
    }
}
