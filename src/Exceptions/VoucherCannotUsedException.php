<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Exceptions;

use Exception;

final class VoucherCannotUsedException extends Exception
{
    public static function throw(): self
    {
        return new self(trans('vouchers::exception.already_used'));
    }
}
