<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Casts;

use HalcyonLaravelBoilerplate\Vouchers\Helper;
use Illuminate\Contracts\Database\Eloquent\Castable;

final class VoucherComputationCast implements Castable
{
    public static function castUsing(array $arguments): VoucherFormulaCaster
    {
        return new VoucherFormulaCaster(Helper::TYPE_COMPUTATIONS);
    }
}
