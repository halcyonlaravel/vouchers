<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Casts;

use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Helper;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

final class VoucherFormulaCaster implements CastsAttributes
{
    public function __construct(private string $type)
    {
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  mixed  $value
     * @return mixed
     *
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException
     */
    public function get($model, string $key, $value, array $attributes)
    {
        $class = Helper::getClasses($this->type)
            ->filter(fn ($class) => $class::name() == $value)
            ->first();

        if (blank($class)) {
            ValidationException::throw(__METHOD__.': no class found');
        }

        return new $class($model);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  mixed  $value
     *
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException
     */
    public function set($model, string $key, $value, array $attributes): string
    {
        //        $type = Helper::interfaceName($this->type);
        //
        //        if ($value instanceof $type ||
        //            (is_string($value) && class_exists($value) && is_subclass_of($value, $type))) {
        //            $value = $value::name();
        //        }

        return Helper::getClassName($value, $this->type);
    }
}
