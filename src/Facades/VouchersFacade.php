<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Facades;

use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin \HalcyonLaravelBoilerplate\Vouchers\VoucherManager
 */
class VouchersFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return VoucherManager::class;
    }
}
