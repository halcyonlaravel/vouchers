<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\ComputationContract;
use HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\RequirementsContract;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Collection;

final class Helper
{
    public const TYPE_COMPUTATIONS = 'computations';

    public const TYPE_REQUIREMENTS = 'requirements';

    /**
     * @return \Illuminate\Support\Collection<int, class-string>
     *
     * @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException
     */
    public static function getClasses(string $type): Collection
    {
        $interfaceName = self::interfaceName($type);

        /** @var array<int, class-string> $classes */
        $classes = config("vouchers.mapping.$type");

        foreach ($classes as $class) {
            if (! is_subclass_of($class, $interfaceName)) {
                ValidationException::throw("`$class` must implement $interfaceName.");
            }
        }

        return collect($classes);
    }

    /** @return \Illuminate\Support\Collection<int, class-string> */
    public static function getComputationClasses(): Collection
    {
        return self::getClasses(Helper::TYPE_COMPUTATIONS);
    }

    /** @return \Illuminate\Support\Collection<int, class-string> */
    public static function getRequirementClasses(): Collection
    {
        return self::getClasses(Helper::TYPE_REQUIREMENTS);
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    public static function getClassName(string|ComputationContract|RequirementsContract $class, string $type): string
    {
        self::validateClass($class, $type);

        /**
         * ErrorException : class_exists() expects parameter 1 to be string, object given
         */
        return is_object($class) || class_exists($class) ? $class::name() : $class;
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    public static function validateClass(object|string|null $value, string $type): void
    {
        $interfaceName = self::interfaceName($type);

        $tmp = is_object($value) ? $value::class : $value;

        $errorMessage = "`$tmp` must implement $interfaceName.";
        if ($value === null) {
            ValidationException::throw($errorMessage);
        }

        if (is_string($value) && ! class_exists($value)) {
            foreach (self::getClasses($type) as $class) {
                if ($class::name() === $value) {
                    return;
                }
            }
            ValidationException::throw($errorMessage);
        }

        if ($value !== null && is_subclass_of($value, $interfaceName)) {
            return;
        }

        ValidationException::throw($errorMessage);
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    public static function mustOnlyOneAttributeInIncompatible(bool $voucher, bool $computation, bool $batch): void
    {
        if (
            ($voucher && $computation && $batch) ||
            (! $voucher && ! $computation && ! $batch) ||

            (! $voucher && $computation && $batch) ||
            ($voucher && ! $computation && $batch) ||
            ($voucher && $computation && ! $batch)
        ) {
            ValidationException::throw('At least one initialise allowed.');
        }
    }

    public static function arrayHasDuplicate(?array $array): bool
    {
        if (blank($array)) {
            return false;
        }

        $duplicate = [];
        /** @phpstan-ignore-next-line */
        foreach ($array as $val) {
            $v = $duplicate[$val] ?? 0;
            if (++$v > 1) {
                return true;
            }
            $duplicate[$val] = $v;
        }

        return false;
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    public static function interfaceName(string $type): string
    {
        self::checkArg($type);

        return $type === self::TYPE_COMPUTATIONS
            ? ComputationContract::class
            : RequirementsContract::class;
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    private static function checkArg(string $arg): void
    {
        if (! in_array($arg, [self::TYPE_COMPUTATIONS, self::TYPE_REQUIREMENTS])) {
            ValidationException::throw(
                sprintf('Invalid argument, valid is [%s, %s]', self::TYPE_COMPUTATIONS, self::TYPE_REQUIREMENTS)
            );
        }
    }

    public static function getMorphedModel(string $alias): string
    {
        return Relation::getMorphedModel($alias) ?? $alias;
    }
}
