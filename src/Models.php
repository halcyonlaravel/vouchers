<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers;

use HalcyonLaravelBoilerplate\Vouchers\Models\Computation;
use HalcyonLaravelBoilerplate\Vouchers\Models\ComputationModel;
use HalcyonLaravelBoilerplate\Vouchers\Models\Limit;
use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

final class Models
{
    private function __construct()
    {
    }

    public static function computation(): Computation
    {
        return app(config('vouchers.models.computation', Computation::class));
    }

    public static function computationModel(): ComputationModel
    {
        return app(config('vouchers.models.computation_model', ComputationModel::class));
    }

    public static function limit(): Limit
    {
        return app(config('vouchers.models.limit', Limit::class));
    }

    public static function requirement(): Requirement
    {
        return app(config('vouchers.models.requirement', Requirement::class));
    }

    public static function voucher(): Voucher
    {
        return app(config('vouchers.models.voucher', Voucher::class));
    }
}
