<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Models;

use HalcyonLaravelBoilerplate\Vouchers\Casts\VoucherComputationCast;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string|null $value
 * @property \HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\ComputationContract $class class name
 * @property-read \Illuminate\Database\Eloquent\Collection|ComputationModel[] $computationModels
 * @property-read \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher $voucher
 * @property \HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\RequirementsContract $requirement class names
 *
 * @method static Builder|Limit whereCode($value)
 * @method static Builder|Limit whereUserableType($value)
 * @method static Builder|Limit whereUserableId($value)
 * @method static Builder|Limit used()
 */
class Computation extends Model
{
    protected $fillable = [
        'voucher_id',
        'value',
        'class',
    ];

    protected $casts = [
        'class' => VoucherComputationCast::class,
    ];

    public function getTable(): string
    {
        return config('vouchers.tables.computations', parent::getTable());
    }

    public function voucher(): BelongsTo
    {
        return $this->belongsTo(config('vouchers.models.voucher', Voucher::class));
    }

    public function computationModels(): HasMany
    {
        return $this->hasMany(config('vouchers.models.computation_model', ComputationModel::class), 'computation_id');
    }
}
