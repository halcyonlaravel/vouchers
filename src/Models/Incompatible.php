<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Models;

use HalcyonLaravelBoilerplate\Vouchers\Helper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int|null $voucher_id
 * @property int|null $incompatible_voucher_id
 * @property string|null $computation_class_name
 * @property int|null $batch
 * @property-read \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher|null $voucher
 * @property-read \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher|null $incompatibleVoucher
 *
 * @method static Builder|Incompatible whereComputationClassName($value)
 * @method static Builder|Incompatible whereBatch($value)
 */
class Incompatible extends Model
{
    protected $fillable = [
        'voucher_id',
        'incompatible_voucher_id',
        'computation_class_name',
        'batch',
    ];

    protected static function booted(): void
    {
        parent::booted();

        static::creating(
            function (self $incompatible) {
                $voucher = filled($incompatible->incompatible_voucher_id);
                $computation = filled($incompatible->computation_class_name);
                $batch = filled($incompatible->batch);

                Helper::mustOnlyOneAttributeInIncompatible($voucher, $computation, $batch);
            }
        );
    }

    public function getTable(): string
    {
        return config('vouchers.tables.incompatibles', parent::getTable());
    }

    public function voucher(): BelongsTo
    {
        return $this->belongsTo(config('vouchers.models.voucher', Voucher::class));
    }

    public function incompatibleVoucher(): BelongsTo
    {
        return $this->belongsTo(
            config('vouchers.models.voucher', Voucher::class),
            'incompatible_voucher_id',
        );
    }
}
