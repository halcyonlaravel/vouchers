<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Models;

use HalcyonLaravelBoilerplate\Vouchers\Actions\GetRequirementsDataArrayAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\GetVoucherBatchCollectionAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\GetVoucherComputationModelModelsArrayAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\GetVoucherPrefixAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\GetVoucherUsedCountAction;
use HalcyonLaravelBoilerplate\Vouchers\Actions\GetVoucherUsedOrderAction;
use Illuminate\Database\Eloquent\Collection;

trait VoucherTrait
{
    public function getPrefix(): string
    {
        return app(GetVoucherPrefixAction::class)->execute($this);
    }

    public function eligibilityTranslated(): string
    {
        return trans("vouchers::label.eligibilities.{$this->eligibility}");
    }

    public function requirementsDataArray(): array
    {
        return app(GetRequirementsDataArrayAction::class)->execute($this);
    }

    public function getQuantity(): int
    {
        return static::whereBatch($this->batch)->count('batch');
    }

    public function isBulk(): bool
    {
        return $this->getQuantity() > 1;
    }

    public function getUsedCount(bool $bulk = false): int
    {
        return app(GetVoucherUsedCountAction::class)->execute($this, $bulk);
    }

    public function getComputationModelModelsArray(): array
    {
        return app(GetVoucherComputationModelModelsArrayAction::class)->execute($this);
    }

    /** @return \Illuminate\Database\Eloquent\Collection|\HalcyonLaravelBoilerplate\Vouchers\Models\Voucher[] */
    public function fetchBatch(string $status = self::STATUS_ALL, array $columns = ['code'], array $with = []): Collection
    {
        return app(GetVoucherBatchCollectionAction::class)->execute(
            $this,
            $status,
            $columns,
            $with
        );
    }

    public function usedOrders(
        string $status = self::STATUS_USED,
        array $with = []
    ): array {
        return app(GetVoucherUsedOrderAction::class)
            ->execute($this, $status, $with);
    }
}
