<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @method static Builder|Limit whereCode($value)
 * @method static Builder|Limit whereUserableType($value)
 * @method static Builder|Limit whereUserableId($value)
 * @method static Builder|Limit whereOrderableType($value)
 * @method static Builder|Limit whereOrderableId($value)
 * @method static Builder|Limit used()
 *
 * @property-read \HalcyonLaravelBoilerplate\Vouchers\Contracts\OrderableLimitVoucherContract|null $orderable
 * @property-read \HalcyonLaravelBoilerplate\Vouchers\Contracts\UserableLimitVoucherContract|null $userable
 */
class Limit extends Model
{
    protected $fillable = [
        'voucher_id',
        'userable_type',
        'userable_id',
        'orderable_type',
        'orderable_id',
        'code',
    ];

    public static function getUsedCountByVoucherCode(string $code): int
    {
        return static::getUsedCountByVoucherCodeQuery($code)->count('code');
    }

    public static function getUsedCountByVoucherCodeQuery(string $code): \Illuminate\Database\Query\Builder
    {
        return static::whereCode($code)
            ->used()
            ->selectRaw('sum(used)')
            ->getQuery();
    }

    public function getTable()
    {
        return config('vouchers.tables.limits', parent::getTable());
    }

    public function scopeUsed(Builder $query): Builder
    {
        return $query->where('used', true);
    }

    public function orderable(): MorphTo
    {
        return $this->morphTo();
    }

    public function userable(): MorphTo
    {
        return $this->morphTo();
    }
}
