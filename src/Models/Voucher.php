<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property string $code
 * @property string|null $title
 * @property string|null $description
 * @property int $batch
 * @property int $limit_usage zero is unlimited
 * @property int $limit_per_user zero is unlimited
 * @property string $eligibility who is eligibility to use
 * @property string|null $remarks for admin use only
 * @property \Illuminate\Support\Carbon $valid_start_at
 * @property \Illuminate\Support\Carbon|null $valid_end_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection $users
 * @property-read int|null $users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\HalcyonLaravelBoilerplate\Vouchers\Models\Requirement[] $requirements
 * @property-read \Illuminate\Database\Eloquent\Collection|\HalcyonLaravelBoilerplate\Vouchers\Models\Incompatible[] $incompatibles
 * @property-read \HalcyonLaravelBoilerplate\Vouchers\Models\Computation $computation
 * @property-read \Illuminate\Database\Eloquent\Collection|\HalcyonLaravelBoilerplate\Vouchers\Models\Limit[] $limits
 * @property-read int|null $limits_count
 * @property-read int|null $requirements_count
 *
 * @method static Builder|Voucher newModelQuery()
 * @method static Builder|Voucher newQuery()
 * @method static Builder|Voucher query()
 * @method static Builder|Voucher whereBatch($value)
 * @method static Builder|Voucher whereCode($value)
 * @method static Builder|Voucher whereComputation($value)
 * @method static Builder|Voucher whereComputationValue($value)
 * @method static Builder|Voucher whereCreatedAt($value)
 * @method static Builder|Voucher whereDescription($value)
 * @method static Builder|Voucher whereEligibility($value)
 * @method static Builder|Voucher whereId($value)
 * @method static Builder|Voucher whereLimit($value)
 * @method static Builder|Voucher whereLimitPerUser($value)
 * @method static Builder|Voucher whereRemarks($value)
 * @method static Builder|Voucher whereTitle($value)
 * @method static Builder|Voucher whereUpdatedAt($value)
 * @method static Builder|Voucher whereValidEndAt($value)
 * @method static Builder|Voucher whereValidStartAt($value)
 */
class Voucher extends Model
{
    use VoucherTrait;

    public const STATUS_ALL = 'all';

    public const STATUS_USED = 'used';

    public const STATUS_UNUSED = 'unused';

    public const STATUSES = [
        self::STATUS_ALL,
        self::STATUS_USED,
        self::STATUS_UNUSED,
    ];

    public const MINIMUM_LIMIT_USAGE = 0;

    public const ELIGIBILITY_EVERYONE = 'everyone';

    public const ELIGIBILITY_REGISTER_CUSTOMERS = 'registered_customers';

    public const ELIGIBILITY_SPECIFIC_CUSTOMERS = 'specific_customers';

    public const ELIGIBILITIES = [
        self::ELIGIBILITY_EVERYONE,
        self::ELIGIBILITY_REGISTER_CUSTOMERS,
        self::ELIGIBILITY_SPECIFIC_CUSTOMERS,
    ];

    protected $fillable = [
        'code',
        'title',
        'description',
        'batch',
        'limit_usage',
        'limit_per_user',
        'eligibility',
        'remarks',
        'valid_start_at',
        'valid_end_at',
    ];

    protected $casts = [
        'limit_per_user' => 'int',
        'limit_usage' => 'int',
        'batch' => 'int',
        'valid_start_at' => 'datetime',
        'valid_end_at' => 'datetime',
    ];

    protected static function booted(): void
    {
        parent::booted();

        static::saving(
            function (self $voucher) {
                $voucher->code = str_replace(' ', '_', $voucher->code);
            }
        );

        static::deleting(
            function (self $voucher) {
                $voucher->load([
                    'requirements',
                    'computation',
                ]);

                if ($voucher->getUsedCount() > 0) {
                    abort(422, trans('vouchers::exception.cannot_destroy'));
                    //                ValidationException::throw('Cannot delete promo, already in use.');
                }

                if ($voucher->requirements->count() > 0) {
                    $voucher->requirements->each->delete();
                }
                if ($voucher->computation->computationModels->count() > 0) {
                    $voucher->computation->computationModels->each->delete();
                }
                $voucher->computation->delete();
            }
        );
    }

    public function getTable(): string
    {
        return config('vouchers.tables.vouchers', parent::getTable());
    }

    public function getRouteKeyName(): string
    {
        return 'code';
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            config('vouchers.models.user'),
            config('vouchers.tables.user_voucher'),
            'voucher_id',
            'user_id',
        );
    }

    public function requirements(): HasMany
    {
        return $this->hasMany(config('vouchers.models.requirement', Requirement::class), 'voucher_id');
    }

    public function computation(): HasOne
    {
        return $this->hasOne(config('vouchers.models.computation', Computation::class), 'voucher_id');
    }

    public function limits(): HasMany
    {
        return $this->hasMany(config('vouchers.models.limit', Limit::class), 'voucher_id');
    }

    public function incompatibles(): HasMany
    {
        return $this->hasMany(config('vouchers.models.incompatible', Incompatible::class), 'voucher_id');
    }
}
