<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Models;

use HalcyonLaravelBoilerplate\Vouchers\Casts\VoucherRequirementsCast;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property string|null $value
 * @property-read \Illuminate\Database\Eloquent\Model|\HalcyonLaravelBoilerplate\Vouchers\Traits\HasVoucherTrait|null $model
 * @property-read \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher $voucher
 * @property \HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\RequirementsContract $class class names
 *
 * @method static Builder|Limit whereCode($value)
 * @method static Builder|Limit whereUserableType($value)
 * @method static Builder|Limit whereUserableId($value)
 * @method static Builder|Limit used()
 */
class Requirement extends Model
{
    protected $fillable = [
        'voucher_id',
        'model_type',
        'model_id',
        'value',
        'class',
    ];

    protected $casts = [
        'class' => VoucherRequirementsCast::class,
    ];

    public function getTable(): string
    {
        return config('vouchers.tables.requirements', parent::getTable());
    }

    public function voucher(): BelongsTo
    {
        return $this->belongsTo(config('vouchers.models.voucher', Voucher::class));
    }

    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
