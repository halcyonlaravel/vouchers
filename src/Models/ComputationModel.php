<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property-read \HalcyonLaravelBoilerplate\Vouchers\Models\Computation $computation
 * @property-read \Illuminate\Database\Eloquent\Model|\HalcyonLaravelBoilerplate\Vouchers\Contracts\HasVoucherComputationFreeItem $model
 */
class ComputationModel extends Model
{
    protected $fillable = [
        'computation_id',
        'model_type',
        'model_id',
    ];

    public function getTable(): string
    {
        return config('vouchers.tables.computation_models', parent::getTable());
    }

    public function computation(): BelongsTo
    {
        return $this->belongsTo(config('vouchers.models.computation', Computation::class));
    }

    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
