<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Traits;

use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasVoucherTrait
{
    public function voucherRequirements(): MorphMany
    {
        return $this->morphMany(Requirement::class, 'model');
    }

    abstract public function voucherTitle(): string;
}
