<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Helpers;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData;

/**
 * @deprecated use \HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData
 */
final class IncompatibleHelper extends IncompatibleData
{
}
