<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Helpers;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;

/**
 * @deprecated use \HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData
 */
final class ComputationHelper extends ComputationData
{
}
