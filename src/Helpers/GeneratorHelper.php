<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Helpers;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\CodeGeneratorData;

/**
 * @deprecated use ComputationData
 */
final class GeneratorHelper extends CodeGeneratorData
{
}
