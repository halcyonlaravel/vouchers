<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Helpers;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;

/**
 * @deprecated use \HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData
 */
final class RequirementHelper extends RequirementData
{
}
