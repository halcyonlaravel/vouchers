<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Helpers;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;

/**
 * @deprecated use \HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData
 */
final class VoucherUpdateHelper extends VoucherUpdateData
{
}
