<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Helpers;

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;

/**
 * @deprecated use \HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData
 */
final class VoucherHelper extends VoucherData
{
}
