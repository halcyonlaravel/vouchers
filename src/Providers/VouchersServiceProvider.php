<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Providers;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class VouchersServiceProvider extends PackageServiceProvider
{
    protected function getPackageBaseDir(): string
    {
        return __DIR__.DIRECTORY_SEPARATOR.'..';
    }

    public function configurePackage(Package $package): void
    {
        $package
            ->name('vouchers')
            ->hasTranslations()
            ->hasMigration('create_vouchers_table')
            ->hasConfigFile('vouchers');
    }
}
