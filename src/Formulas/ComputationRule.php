<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Formulas;

class ComputationRule
{
    public bool $canMultipleUse = false;

    /** @return \HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\ComputationContract[] */
    public array $incompatibleWith = [];
}
