<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Formulas;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\ComputationContract;
use HalcyonLaravelBoilerplate\Vouchers\Models\Computation;

abstract class BaseVoucherComputation extends BaseFormula implements ComputationContract
{
    public function __construct(protected Computation $class)
    {
    }

    /** @return non-empty-string */
    public static function label(): string
    {

        /** @phpstan-ignore-next-line  */
        return trans('vouchers::label.computations.'.static::name());
    }

    public function rules(): ComputationRule
    {
        return new ComputationRule();
    }

    /** @return non-empty-string */
    public static function typeLabel(): string
    {
        /** @phpstan-ignore-next-line  */
        return trans('vouchers::type.computations.'.static::type());
    }
}
