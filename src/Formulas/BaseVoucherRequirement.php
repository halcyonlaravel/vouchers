<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Formulas;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\RequirementsContract;
use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;

abstract class BaseVoucherRequirement extends BaseFormula implements RequirementsContract
{
    public function __construct(protected Requirement $class)
    {
    }

    /** @return non-empty-string */
    public static function label(): string
    {
        /** @phpstan-ignore-next-line  */
        return trans('vouchers::label.requirements.'.static::name());
    }

    /** @return non-empty-string */
    public function validationMessage(): string
    {
        return static::class;
    }
}
