<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Formulas;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\BaseContract;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

abstract class BaseFormula implements BaseContract
{
    public function voucher(): Voucher
    {
        /** @phpstan-ignore-next-line */
        return $this->class->voucher;
    }
}
