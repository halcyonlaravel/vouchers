<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\RequirementsContract;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Helper;
use HalcyonLaravelBoilerplate\Vouchers\Traits\HasVoucherTrait;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;

class RequirementData extends BaseData
{
    /** @var RequirementsContract|string */
    public $class;

    public ?Model $model = null;

    /** @var mixed|null */
    public $value = null;

    /** @param  mixed|null  $modelOrValue */
    public function __construct(string $class = null, $modelOrValue = null)
    {
        if (filled($modelOrValue) && is_null($class)) {
            throw new InvalidArgumentException(
                self::class.' __construct argument 1 is required when argument 2 is assigned.'
            );
        }

        if ($modelOrValue instanceof Model) {
            $this->model = $modelOrValue;
        } else {
            $this->value = $modelOrValue;
        }

        if ($class !== null) {
            $this->class = $class;
        }
    }

    /** {@inheritDoc} */
    public function validate(): void
    {
        Helper::validateClass($this->class, Helper::TYPE_REQUIREMENTS);

        if ((! is_null($this->model) && ! is_null($this->value)) ||
            (is_null($this->model) && is_null($this->value))
        ) {
            ValidationException::throw(
                '`model` and `value` must not initialise or null both at the same time in '.self::class
            );
        }

        if ($this->model !== null && ! is_class_uses_deep($this->model, HasVoucherTrait::class)) {
            ValidationException::throw(
                'Model `'.Helper::getMorphedModel(
                    $this->model->getMorphClass()
                ).'` must uses trait '.HasVoucherTrait::class.'.'
            );
        }
    }
}
