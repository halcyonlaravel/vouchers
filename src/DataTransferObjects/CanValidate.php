<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

interface CanValidate
{
    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    public function validate(): void;
}
