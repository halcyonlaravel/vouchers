<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

abstract class BaseData implements CanValidate
{
    protected static function resolveClass(string|object|null $class): string
    {
        if ($class === null) {
            return '';
        }

        return is_string($class) ? $class : $class::class;
    }
}
