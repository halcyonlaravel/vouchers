<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

use Carbon\Carbon;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Foundation\Auth\User;

abstract class BaseVoucherData extends BaseData
{
    public ?CodeGeneratorData $generator_helper = null;

    public Carbon $valid_start_at;

    /**
     * limit per voucher
     */
    public int $limit = Voucher::MINIMUM_LIMIT_USAGE;

    public int $limit_per_user = Voucher::MINIMUM_LIMIT_USAGE;

    public ?Carbon $valid_end_at = null;

    public string $eligibility = Voucher::ELIGIBILITY_EVERYONE;

    public ?string $title = null;

    public ?string $code = null;

    public ?string $description = null;

    public ?string $remarks = null;

    /** @var \HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData[]|null */
    public ?array $requirements = null;

    public ComputationData $computation;

    /** @var array<int, \Illuminate\Database\Eloquent\Model>|null */
    public ?array $users = null;

    public ?IncompatibleData $incompatibleHelper = null;

    /** {@inheritDoc} */
    public function validate(): void
    {
        if (! is_null($this->code)) {
            $this->generator_helper ??= new CodeGeneratorData();
            $code = preg_replace(
                "/[^a-zA-Z0-9_{$this->generator_helper->separator}]/",
                '',
                str_replace(' ', '_', $this->code)
            );
            if (blank($code)) {
                ValidationException::throw('Code is empty after removing spacial characters');
            }
            $this->code = $code;
        }

        $this->validatedClasses();

        //        if (blank($this->limit) && blank($this->limit_per_user)) {
        //            ValidationException::throw('Required at least limit or limit_per_user.');
        //        }

        if (/*filled($this->limit) &&*/ $this->limit < Voucher::MINIMUM_LIMIT_USAGE) {
            ValidationException::throw('The `limit` must not below '.Voucher::MINIMUM_LIMIT_USAGE.'.');
        }
        if (/*filled($this->limit_per_user) &&*/ $this->limit_per_user < Voucher::MINIMUM_LIMIT_USAGE) {
            ValidationException::throw('The `limit_per_user` must not below '.Voucher::MINIMUM_LIMIT_USAGE.'.');
        }
        if ($this->limit != 0 && $this->limit < $this->limit_per_user) {
            ValidationException::throw(
                'If `limit` is not `zero(0)`, the `limit` must grater than or equal in `limit_per_user`.'
            );
        }

        //        switch ($this->eligibility) {
        //            case Voucher::ELIGIBILITY_EVERYONE:
        //                if (blank($this->limit) || blank($this->limit_per_user)) {
        //                    ValidationException::throw('Required both limit or limit_per_user.');
        //                }
        //                break;
        //            case Voucher::ELIGIBILITY_SPECIFIC_CUSTOMERS:
        //            case Voucher::ELIGIBILITY_REGISTER_CUSTOMERS:
        //                if (blank($this->limit_per_user)) {
        //                    ValidationException::throw('Required limit_per_user.');
        //                }
        //                $this->limit = null;
        //                break;
        //        }

        foreach ($this->users ?? [] as $user) {
            if (! is_subclass_of($user, User::class)) {
                ValidationException::throw('User models must subclass of '.User::class);
            }
        }

        if (! is_null($this->valid_end_at)) {
            if ($this->valid_start_at->greaterThan($this->valid_end_at)) {
                ValidationException::throw('`valid_end_at` must be greater than `valid_start_at`.');
            }
        }

        if (! is_null($this->incompatibleHelper)) {
            $this->incompatibleHelper->validate();
        }
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    private function validatedClasses(): void
    {
        $this->computation->validate();

        foreach ($this->requirements ?: [] as $requirement) {
            if (! $requirement instanceof RequirementData) {
                $class = self::resolveClass($requirement);
                ValidationException::throw("`$class` must instance of ".RequirementData::class.'.');
            }
            $requirement->validate();
        }
    }
}
