<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

class VoucherData extends BaseVoucherData
{
    public ?int $batch = null;
}
