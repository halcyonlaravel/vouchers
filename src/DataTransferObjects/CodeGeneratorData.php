<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

use Lloricode\CheckDigit\Enums\Format;

class CodeGeneratorData
{
    //    public const CASE_UPPER = 'uppercase';
    //    public const CASE_LOWER = 'lowercase';
    //    public const CASE_MIXED = 'mixed_case';
    //    public string $case = self::CASE_UPPER;

    public string $characters = 'ABCDEFGHJKLMNOPQRSTUVWXYZ234567890';

    public string $mask = '****-****';

    public ?string $prefix = null;

    public ?string $suffix = null;

    public string $separator = '-';

    public bool $checkDigit = false;

    public Format $checkDigitFormat;

    public function __construct()
    {
        $this->checkDigitFormat = Format::GTIN_13();
    }

    //    public function getCharacters(): string
    //    {
    //        $chars = '';
    //        switch ($this->case) {
    //            case self::CASE_UPPER:
    //                $chars = strtoupper($this->characters);
    //                break;
    //            case self::CASE_LOWER:
    //                $chars = strtolower($this->characters);
    //                break;
    //            // @codeCoverageIgnoreStart
    //            case self::CASE_MIXED:
    //                // TODO: add logic here
    //                break;
    //            // @codeCoverageIgnoreEnd
    //        }
    //
    //        return $chars;
    //    }
}
