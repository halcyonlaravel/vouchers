<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Helper;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

class IncompatibleData extends BaseData
{
    /** @var \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher[]|null */
    public ?array $vouchers = null;

    /** @var \HalcyonLaravelBoilerplate\Vouchers\Formulas\BaseVoucherComputation[]|string[]|null */
    public ?array $computations = null;

    /** @var int[]|null */
    public ?array $voucher_batches = null;

    public static function sort(array &$array, bool $sorByKey = false): void
    {
        if ($sorByKey) {
            ksort($array);
        } else {
            sort($array);
        }
    }

    private static function isHasDuplicate(?array $array): bool
    {
        return Helper::arrayHasDuplicate($array);
    }

    public function validate(): void
    {
        Helper::mustOnlyOneAttributeInIncompatible(
            filled($this->vouchers),
            filled($this->computations),
            filled($this->voucher_batches)
        );

        $this->checkVoucher();
        $this->checkComputation();
        $this->checkBatch();
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    private function checkVoucher(): void
    {
        foreach ($this->vouchers ?: [] as $voucher) {
            if (! $voucher instanceof Voucher) {
                ValidationException::throw(sprintf('%s: voucher must instance of %s.', self::class, Voucher::class));
            }
        }

        if (self::isHasDuplicate(collect($this->vouchers)->map(fn (Voucher $v) => $v->getKey())->toArray())) {
            ValidationException::throw(sprintf('Duplicate value found in `%s` in `%s`.', 'vouchers', self::class));
        }
        if (filled($this->vouchers)) {
            $new = [];
            /** @phpstan-ignore-next-line */
            foreach ($this->vouchers as $voucher) {
                $new[$voucher->getKey()] = $voucher;
            }
            $this->vouchers = $new;
            self::sort($this->vouchers, true);
        }
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    private function checkComputation(): void
    {
        if (filled($this->computations)) {
            $new = [];
            /** @phpstan-ignore-next-line */
            foreach ($this->computations as $computation) {
                $new[] = Helper::getClassName($computation, Helper::TYPE_COMPUTATIONS);
            }
            $this->computations = $new;
            self::sort($this->computations);
        }

        if (self::isHasDuplicate($this->computations)) {
            ValidationException::throw(sprintf('Duplicate value found in `%s` in `%s`.', 'computations', self::class));
        }
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    private function checkBatch(): void
    {
        /** @var \HalcyonLaravelBoilerplate\Vouchers\Models\Voucher $VOUCHER_MODEL */
        $VOUCHER_MODEL = config('vouchers.models.voucher', Voucher::class);

        foreach ($this->voucher_batches ?: [] as $batch) {
            if (! is_int($batch)) {
                ValidationException::throw(sprintf('%s: voucher_batches must type of int.', self::class));
            }
            if ($VOUCHER_MODEL::whereBatch($batch)->count('batch') == 0) {
                ValidationException::throw(sprintf('%s: voucher_batches value is not exist.', self::class));
            }
        }

        if (self::isHasDuplicate($this->voucher_batches)) {
            ValidationException::throw(
                sprintf('Duplicate value found in `%s` in `%s`.', 'voucher_batches', self::class)
            );
        }
        if (filled($this->voucher_batches)) {
            self::sort($this->voucher_batches);
        }
    }
}
