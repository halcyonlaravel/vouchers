<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;

class VoucherUpdateData extends BaseVoucherData
{
    use HasAttributes;

    public int $batch;

    private Voucher $voucher;

    public function __construct(Voucher $voucher)
    {
        $voucher->loadMissing([
            'requirements.model',
            'computation.computationModels.model',
        ]);

        $this->voucher = $voucher;

        $this->valid_start_at = $voucher->valid_start_at;
        $this->batch = $voucher->batch;

        $this->limit = $voucher->limit_usage;
        $this->limit_per_user = $voucher->limit_per_user;
        $this->valid_end_at = $voucher->valid_end_at;

        $this->eligibility = $voucher->eligibility;

        $this->code = $this->isBulk()
            ? null
            : $voucher->code;

        $this->title = $voucher->title;
        $this->description = $voucher->description;
        $this->remarks = $voucher->remarks;

        // computation

        $computationModel = $voucher->computation;

        $computationHelper = new ComputationData($computationModel->class::name());
        $computationHelper->setValue($computationModel->value);

        $cModels = [];
        foreach ($voucher->computation->computationModels as $_computationModel) {
            $cModels[] = $_computationModel->model;
        }
        if (filled($cModels)) {
            $computationHelper->setModels($cModels);
        }

        $this->original['computation'] = $computationHelper;
        $this->computation = $computationHelper;

        // requirements

        $requirements = [];
        foreach ($voucher->requirements as $requirement) {
            $requirements[] = new RequirementData(
                $requirement->class::name(),
                $requirement->value ?: $requirement->model
            );
        }

        if (blank($requirements)) {
            $requirements = null;
        }

        $this->original['requirements'] = $requirements;
        $this->requirements = $requirements;

        // users

        $users = [];

        foreach ($voucher->users as $user) {
            $users[] = $user;
        }

        if (blank($users)) {
            $users = null;
        }

        $this->original['users'] = $users;
        $this->users = $users;
    }

    public function isIncompatibleDirty(): bool
    {
        return false;
    }

    public function isComputationDirty(): bool
    {
        $this->attributes['computation'] = $this->computation;

        return $this->isDirty('computation');
    }

    public function isRequirementsDirty(): bool
    {
        $this->attributes['requirements'] = $this->requirements;

        return $this->isDirty('requirements');
    }

    public function isUsersDirty(): bool
    {
        $this->attributes['users'] = $this->users;

        return $this->isDirty('users');
    }

    public function isBulk(): bool
    {
        return $this->voucher->isBulk();
    }

    /** {@inheritDoc} */
    public function validate(): void
    {
        if ($this->isBulk() && filled($this->code)) {
            ValidationException::throw('Code must be null if voucher is bulk created.');
        }
        parent::validate();
    }

    /**
     * for HasAttributes compatibility
     *
     * @return false
     */
    public function usesTimestamps(): bool
    {
        return false;
    }

    /**
     * for HasAttributes compatibility
     *
     * @return false
     */
    public function getIncrementing(): bool
    {
        return false;
    }
}
