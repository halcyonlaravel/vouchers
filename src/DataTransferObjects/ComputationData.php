<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\ComputationContract;
use HalcyonLaravelBoilerplate\Vouchers\Contracts\HasVoucherComputationFreeItem;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Helper;
use Illuminate\Database\Eloquent\Model;

class ComputationData extends BaseData
{
    public ComputationContract|string $class;

    public ?string $value = null;

    /** @var array<int, Model|HasVoucherComputationFreeItem>|null */
    public ?array $models = null;

    public function __construct(string $class = null)
    {
        if (! is_null($class)) {
            $this->class = $class;
        }
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function setModels(array $models): self
    {
        $this->models = $models;

        return $this;
    }

    /** {@inheritDoc} */
    public function validate(): void
    {
        if ($this->value === null && $this->models === null) {
            ValidationException::throw('Required `value` or `models` or both in '.self::class);
        }

        $this->validatedClasses();
    }

    /** @throws \HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException */
    private function validatedClasses(): void
    {
        Helper::validateClass($this->class, Helper::TYPE_COMPUTATIONS);

        foreach ($this->models ?? [] as $model) {
            if (
                ! is_subclass_of($model, Model::class) ||
                ! ($model instanceof HasVoucherComputationFreeItem)
            ) {
                ValidationException::throw(
                    self::class.'::models must subclass of '.Model::class.
                    ' and implements '.HasVoucherComputationFreeItem::class.'.'
                );
            }
        }
    }
}
