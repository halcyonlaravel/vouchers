<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Contracts;

/**
 * @mixin \Illuminate\Database\Eloquent\Model
 */
interface UserableLimitVoucherContract
{
    /** @return non-empty-string */
    public function voucherLimitTitle(): string;
}
