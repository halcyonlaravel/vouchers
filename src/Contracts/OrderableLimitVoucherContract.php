<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Contracts;

/**
 * @mixin \Illuminate\Database\Eloquent\Model
 */
interface OrderableLimitVoucherContract
{
    /** @return non-empty-string */
    public function voucherLimitTitle(): string;
}
