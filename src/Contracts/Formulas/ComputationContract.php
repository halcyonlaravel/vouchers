<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas;

use HalcyonLaravelBoilerplate\Vouchers\Formulas\ComputationRule;
use Money\Money;

interface ComputationContract extends BaseContract
{
    /** @return non-empty-string */
    public static function type(): string;

    /** @return non-empty-string */
    public static function typeLabel(): string;

    public function discountPrice(Money $price = null): Money;

    public function rules(): ComputationRule;
}
