<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas;

interface RequirementsContract extends BaseContract
{
    public function validate(): bool;
}
