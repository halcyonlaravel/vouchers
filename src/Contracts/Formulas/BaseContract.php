<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas;

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;

interface BaseContract
{
    /** @return non-empty-string */
    public static function name(): string;

    /** @return non-empty-string */
    public static function label(): string;

    public function voucher(): Voucher;
}
