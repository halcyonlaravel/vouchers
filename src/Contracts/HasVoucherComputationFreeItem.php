<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Contracts;

/**
 * @mixin \Illuminate\Database\Eloquent\Model
 */
interface HasVoucherComputationFreeItem
{
    /** @return non-empty-string */
    public function voucherFreeItemTitle(): string;
}
