# Voucher for laravel boilerplate

## Installation

You can install the package via composer:

```bash
composer require HalcyonLaravelBoilerplate/vouchers
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="HalcyonLaravelBoilerplate\Vouchers\VouchersServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:
```bash
php artisan vendor:publish --provider="HalcyonLaravelBoilerplate\Vouchers\VouchersServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```

## Usage

``` php
$vouchers = new HalcyonLaravelBoilerplate\Vouchers();
echo $vouchers->echoPhrase('Hello, HalcyonLaravelBoilerplate!');
```

## Testing

``` bash
vendor/bin/phpunit
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.


