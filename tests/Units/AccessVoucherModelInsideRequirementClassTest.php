<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

it('access', function () {
    $vh = prepareVoucherHelper();
    $vh->requirements = [new RequirementData(TestBasicVoucherRequirements::name(), 'ok')];
    //        $vh->extra = ['test_bool_value' => true];

    $voucher = VoucherManager::create($vh)->refresh();

    assertTrue($voucher->requirements[0]->class->validate());

    $vh = prepareVoucherHelper();
    $vh->requirements = [new RequirementData(TestBasicVoucherRequirements::name(), 'not ok')];
    //        $vh->extra = ['test_bool_value' => false];

    $voucher = VoucherManager::create($vh)->refresh();

    assertFalse($voucher->requirements[0]->class->validate());
});
