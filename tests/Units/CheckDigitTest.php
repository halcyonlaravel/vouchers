<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\CodeGeneratorData;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function PHPUnit\Framework\assertMatchesRegularExpression;
use function PHPUnit\Framework\assertStringStartsWith;

it('generate', function () {
    $voucherHelper = prepareVoucherHelper();

    $generatorHelper = new CodeGeneratorData();
    $generatorHelper->checkDigit = true;
    $generatorHelper->prefix = '33652';

    $voucherModel = VoucherManager::create($voucherHelper, $generatorHelper);

    assertStringStartsWith('33652', $voucherModel->code);
    assertMatchesRegularExpression('/^[0-9]{13}$/', $voucherModel->code);
});
