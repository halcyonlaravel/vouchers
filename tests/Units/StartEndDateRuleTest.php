<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

it('invalid end', function () {
    request()->merge(['end_field' => 'xxx']);

    $v = VoucherManager::startEndDateRule('end_field');

    assertFalse($v->passes('', ''));
    assertEquals('The `end_field` is invalid.', $v->message());
});

it('invalid start', function () {
    //        request()->merge(['end_field' => 'xxx']);

    $v = VoucherManager::startEndDateRule('end_field');

    assertFalse($v->passes('', 'xxx'));
    assertEquals('The :attribute is invalid.', $v->message());
});

it('valid start', function () {
    //        request()->merge(['end_field' => 'xxx']);

    $v = VoucherManager::startEndDateRule('end_field');

    assertTrue($v->passes('', now()));
});

it('invalid start with end', function () {
    $start = now();
    request()->merge(['end_field' => $start->copy()->addDays(-1)]);

    $v = VoucherManager::startEndDateRule('end_field');

    assertFalse($v->passes('', $start));
    assertEquals('The :attribute is invalid.', $v->message());
});

it('valid start with end', function () {
    $start = now();
    request()->merge(['end_field' => $start->copy()->addSecond()]);

    $v = VoucherManager::startEndDateRule('end_field');

    assertTrue($v->passes('', $start));
});
