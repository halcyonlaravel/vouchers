<?php

declare(strict_types=1);

/**
 * https://github.com/beyondcode/laravel-vouchers/blob/1.2.0/tests/GeneratorTest.php
 */

use HalcyonLaravelBoilerplate\Vouchers\VoucherGenerator;

use function PHPUnit\Framework\assertMatchesRegularExpression;
use function PHPUnit\Framework\assertStringEndsWith;
use function PHPUnit\Framework\assertStringStartsWith;

it('uses specified characters only', function () {
    $generator = new VoucherGenerator('1234567890', '********');
    $voucher = $generator->generate();

    assertMatchesRegularExpression('/^[0-9]/', $voucher);
});

it('uses the prefix', function () {
    $generator = new VoucherGenerator('1234567890', '********');
    $generator->setPrefix('beyondcode');

    $voucher = $generator->generate();

    assertStringStartsWith('beyondcode-', $voucher);
});

it('uses the suffix', function () {
    $generator = new VoucherGenerator('1234567890', '********');
    $generator->setSuffix('beyondcode');

    $voucher = $generator->generate();

    assertStringEndsWith('-beyondcode', $voucher);
});

it('uses custom separators', function () {
    $generator = new VoucherGenerator('1234567890', '********');
    $generator->setSeparator('%');
    $generator->setPrefix('beyondcode');
    $generator->setSuffix('beyondcode');

    $voucher = $generator->generate();

    assertStringStartsWith('beyondcode%', $voucher);
    assertStringEndsWith('%beyondcode', $voucher);
});

it('generates code with mask', function () {
    $generator = new VoucherGenerator('ABCDEFGH', '* * * *');
    $voucher = $generator->generate();

    assertMatchesRegularExpression('/(.*)\s(.*)\s(.*)\s(.*)/', $voucher);
});
