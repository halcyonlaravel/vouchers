<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models\Incompatible;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestCanMultipleBasicBaseVoucherComputation;
use Illuminate\Database\Eloquent\Factories\Sequence;

use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;

dataset('success_dataset', [
    [true, false, false],
    [false, true, false],
    [false, false, true],
]);
dataset('failed_dataset', [
    [true, true, true],
    [false, false, false],
    [true, true, false],
    [false, true, true],
    [true, false, true],
]);

it('check failed', function (bool $v, bool $c, bool $b) {
    $i = new IncompatibleData();

    if ($v) {
        $i->vouchers = [VoucherFactory::new()->createOne()];
    }
    if ($c) {
        $i->computations = [TestBasicBaseVoucherComputation::class];
    }
    if ($b) {
        $i->voucher_batches = [1];
    }

    $i->validate();
})
    ->throws(ValidationException::class)
    ->with('failed_dataset');

it('check model failed', function (bool $v, bool $c, bool $b) {
    $i = new Incompatible();
    $i->voucher_id = VoucherFactory::new()->createOne()->getKey();

    if ($v) {
        $i->incompatible_voucher_id = VoucherFactory::new()->createOne()->getKey();
    }
    if ($c) {
        $i->computation_class_name = TestBasicBaseVoucherComputation::name();
    }
    if ($b) {
        VoucherFactory::new()->createOne(['batch' => 1]);
        $i->batch = 1;
    }

    $i->save();
})
    ->throws(ValidationException::class)
    ->with('failed_dataset');

it('check success', function (bool $v, bool $c, bool $b) {
    $i = new IncompatibleData();

    if ($v) {
        $i->vouchers = [VoucherFactory::new()->createOne()];
    }
    if ($c) {
        $i->computations = [TestBasicBaseVoucherComputation::class];
    }
    if ($b) {
        VoucherFactory::new()->createOne(['batch' => 1]);
        $i->voucher_batches = [1];
    }

    $i->validate();

    assertTrue(true);
})
    ->with('success_dataset');

it('check model success', function (bool $v, bool $c, bool $b) {
    $i = new Incompatible();
    $i->voucher_id = VoucherFactory::new()->createOne()->getKey();

    if ($v) {
        $i->incompatible_voucher_id = VoucherFactory::new()->createOne()->getKey();
    }
    if ($c) {
        $i->computation_class_name = TestBasicBaseVoucherComputation::name();
    }
    if ($b) {
        $i->batch = 1;
    }

    $i->save();

    assertTrue(true);
})
    ->with('success_dataset');

it('duplicate voucher', function () {
    $voucher = VoucherFactory::new()->createOne();

    $i = new IncompatibleData();
    $i->vouchers = [$voucher, $voucher];

    $i->validate();
})
    ->throws(
        ValidationException::class,
        'Duplicate value found in `vouchers` in `HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData`.'
    );

it('duplicate batch', function () {
    VoucherFactory::new()->createOne(['batch' => 1]);

    $i = new IncompatibleData();
    $i->voucher_batches = [1, 1];

    $i->validate();
})
    ->throws(
        ValidationException::class,
        'Duplicate value found in `voucher_batches` in `HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData`.'
    );

it('duplicate computation class name', function () {
    $i = new IncompatibleData();
    $i->computations = [TestBasicBaseVoucherComputation::class, TestBasicBaseVoucherComputation::name()];

    $i->validate();
})
    ->throws(
        ValidationException::class,
        'Duplicate value found in `computations` in `HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData`.'
    );

it('computation class convert to name', function () {
    $i = new IncompatibleData();
    $i->computations = [TestBasicBaseVoucherComputation::class, TestCanMultipleBasicBaseVoucherComputation::name()];

    $i->validate();

    assertCount(2, $i->computations);
    assertEquals('test_basic_computation_formula', TestBasicBaseVoucherComputation::name());
    assertEquals('test_basic_computation_formula_x', TestCanMultipleBasicBaseVoucherComputation::name());

    assertEquals('test_basic_computation_formula', $i->computations[0]);
    assertEquals('test_basic_computation_formula_x', $i->computations[1]);
});

it('sort computation', function () {
    $i = new IncompatibleData();
    $i->computations = [TestCanMultipleBasicBaseVoucherComputation::class, TestBasicBaseVoucherComputation::class];

    $i->validate();

    assertEquals(TestBasicBaseVoucherComputation::name(), $i->computations[0]);
    assertEquals(TestCanMultipleBasicBaseVoucherComputation::name(), $i->computations[1]);
});

it('sort voucher', function () {
    $v1 = VoucherFactory::new()->createOne();
    $v2 = VoucherFactory::new()->createOne();
    $v3 = VoucherFactory::new()->createOne();

    $i = new IncompatibleData();

    $i->vouchers = [$v2, $v3, $v1];

    $i->validate();

    $iv = array_values($i->vouchers);

    assertEquals($v1, $iv[0]);
    assertEquals($v2, $iv[1]);
    assertEquals($v3, $iv[2]);
});

it('sort batch', function () {
    VoucherFactory::new()
        ->count(5)
        ->state(
            new Sequence(
                ['batch' => 1],
                ['batch' => 2],
                ['batch' => 3],
                ['batch' => 4],
                ['batch' => 5],
            )
        )
        ->create();

    $i = new IncompatibleData();

    $i->voucher_batches = [2, 5, 1, 3, 4];

    $i->validate();

    assertEquals(1, $i->voucher_batches[0]);
    assertEquals(2, $i->voucher_batches[1]);
    assertEquals(3, $i->voucher_batches[2]);
    assertEquals(4, $i->voucher_batches[3]);
    assertEquals(5, $i->voucher_batches[4]);
});

it('invalid type voucher', function () {
    $i = new IncompatibleData();
    $i->vouchers = [$this->testUser];
    $i->validate();
})
    ->throws(
        ValidationException::class,
        'HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData: voucher must instance of HalcyonLaravelBoilerplate\Vouchers\Models\Voucher.'
    );

// already type hinted with php 8.0
//it('invalid type computation', function () {
//    $i = new IncompatibleData();
//    $i->computations = [$this->testUser];
//    $i->validate();
//});
//    ->throws(
//        ValidationException::class,
//        '`HalcyonLaravelBoilerplate\Vouchers\Tests\Helpers\Models\TestUser` must implement HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\ComputationContract.'
//    );

it('invalid type batch', function () {
    $i = new IncompatibleData();
    $i->voucher_batches = ['test'];
    $i->validate();
})
    ->throws(
        ValidationException::class,
        'HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData: voucher_batches must type of int.'
    );

it('invalid batch number because not exist in db', function () {
    assertAllEmptyVoucherTable();

    $i = new IncompatibleData();
    $i->voucher_batches = [1];
    $i->validate();
})
    ->throws(
        ValidationException::class,
        'HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\IncompatibleData: voucher_batches value is not exist.'
    );
