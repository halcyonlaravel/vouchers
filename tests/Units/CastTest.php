<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Models\Computation;
use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherComputationFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherRequirementFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;

it('get field', function () {
    /** @var Voucher $voucher */
    $voucher = VoucherFactory::new()
        ->has(VoucherRequirementFactory::new())
        ->has(VoucherComputationFactory::new())
        ->create();

    $requirement = $voucher->requirements->first()->class;
    $computation = $voucher->computation->class;

    assertInstanceOf(TestBasicVoucherRequirements::class, $requirement);
    assertInstanceOf(TestBasicBaseVoucherComputation::class, $computation);

    assertEquals('Test Basic Computation', $computation::label());
    assertEquals('Test Basic Rule', $requirement::label());
});

it('set field with only class name', function () {
    VoucherFactory::new()
        ->has(
            VoucherRequirementFactory::new()
                ->state(
                    [
                        'class' => 'test_basic_rule_formula',
                    ]
                )
        )
        ->has(
            VoucherComputationFactory::new()
                ->state(
                    [
                        'class' => 'test_basic_computation_formula',
                    ]
                )
        )
        ->create();
    assertDatabaseCount(Voucher::class, 1);
    assertDatabaseCount(Requirement::class, 1);
    assertDatabaseCount(Computation::class, 1);

    //        assertDatabaseHas(
    //            (new Voucher::class,
    //            [
    //                'computation' => 'test_basic_computation_formula',
    //            ]
    //        );

    assertDatabaseHas(
        Requirement::class,
        [
            'class' => 'test_basic_rule_formula',
        ]
    );

    assertDatabaseHas(
        Computation::class,
        [
            'class' => 'test_basic_computation_formula',
        ]
    );
});
