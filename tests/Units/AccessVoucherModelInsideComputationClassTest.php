<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;
use Money\Money;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertInstanceOf;

it('access', function () {
    $vh = prepareVoucherHelper();
    $vh->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('123456');

    $voucher = VoucherManager::create($vh)->refresh();

    assertInstanceOf(TestBasicBaseVoucherComputation::class, $voucher->computation->class);

    assertEquals(Money::PHP(123456), $voucher->computation->class->discountPrice());
});
