<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;

use function PHPUnit\Framework\assertTrue;

it('success', function () {
    $item = new VoucherData();
    $item->requirements = [new RequirementData(TestBasicVoucherRequirements::class, 'test value')];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->limit = Voucher::MINIMUM_LIMIT_USAGE;
    $item->limit_per_user = Voucher::MINIMUM_LIMIT_USAGE;

    $item->validate();

    $item = new VoucherData();
    $item->requirements = [new RequirementData(TestBasicVoucherRequirements::class, 'test value')];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->limit = Voucher::MINIMUM_LIMIT_USAGE + 1;
    $item->limit_per_user = Voucher::MINIMUM_LIMIT_USAGE + 1;

    $item->validate();

    assertTrue(true);
});

it('success with class name', function () {
    $item = new VoucherData();
    $item->requirements = [new RequirementData(TestBasicVoucherRequirements::class, 'test value')];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::name()))->setValue('test');
    $item->limit = Voucher::MINIMUM_LIMIT_USAGE;
    $item->limit_per_user = Voucher::MINIMUM_LIMIT_USAGE;

    $item->validate();

    $item = new VoucherData();
    $item->requirements = [new RequirementData(TestBasicVoucherRequirements::class, 'test value')];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::name()))->setValue('test');
    $item->limit = Voucher::MINIMUM_LIMIT_USAGE + 1;
    $item->limit_per_user = Voucher::MINIMUM_LIMIT_USAGE + 1;

    $item->validate();

    assertTrue(true);
});

it('requirements can be empty', function () {
    $item = new VoucherData();
    //        $item->requirements = []; // has default empty array
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->limit = 1;
    $item->limit_per_user = 1;

    $item->validate();

    assertTrue(true);
});

it('requirements invalid name failed', function () {
    $item = new VoucherData();
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->requirements = ['x'];

    $item->validate();
})
    ->throws(
        ValidationException::class,
        '`x` must instance of HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData'
    );

it('empty string class failed', function () {
    $item = new VoucherData();
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->requirements = [''];

    $item->validate();
})
    ->throws(
        ValidationException::class,
        '`` must instance of HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData.'
    );

it('invalid requirement class failed', function () {
    $item = new VoucherData();
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->requirements = [new RequirementData(TestBasicBaseVoucherComputation::class, 'test')];

    $item->validate();
})
    ->throws(
        ValidationException::class,
        '`HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation` must implement HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\RequirementsContract.'
    );

it('computation failed', function () {
    $item = new VoucherData();
    $item->requirements = [TestBasicVoucherRequirements::class];
    $item->computation = (new ComputationData('xx'))->setValue('xx');

    $item->validate();
})
    ->throws(
        ValidationException::class,
        '`xx` must implement HalcyonLaravelBoilerplate\Vouchers\Contracts\Formulas\ComputationContract'
    );

it('invalid limit', function () {
    $item = new VoucherData();
    //        $item->RequirementDatas = [TestBasicVoucherRequirements::class];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->limit = -1;

    $item->validate();
})
    ->throws(
        ValidationException::class,
        'The `limit` must not below 0.'
    );

it('invalid models in computation helper', function () {
    $item = (new ComputationData(TestBasicBaseVoucherComputation::class))->setModels([$this]);

    $item->validate();
})
    ->throws(
        ValidationException::class,
        'HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData::models must subclass of Illuminate\Database\Eloquent\Model and implements HalcyonLaravelBoilerplate\Vouchers\Contracts\HasVoucherComputationFreeItem.'
    );
