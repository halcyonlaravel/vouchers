<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Models\Limit;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherLimitFactory;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

it('unused', function () {
    $code = 'code_123';
    $voucher = VoucherFactory::new()->createOne(
        [
            'code' => $code,
        ]
    );

    assertTrue(VoucherManager::checkLimit($code, $this->testUser));

    VoucherLimitFactory::new()->create(
        [
            'voucher_id' => $voucher->getKey(),
            'code' => $code,
        ]
    );

    assertFalse(VoucherManager::checkLimit($code, $this->testUser));

    VoucherManager::unUseCode($this->testUser, $this->testOrder, $code);

    assertDatabaseHas(
        Limit::class,
        [
            'voucher_id' => $voucher->getKey(),
            'code' => 'code_123',
            'used' => false,
        ]
    );

    assertTrue(VoucherManager::checkLimit($code, $this->testUser));
});
