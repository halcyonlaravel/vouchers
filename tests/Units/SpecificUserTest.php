<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestUserFactory;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

beforeEach(fn () => assertAllEmptyVoucherTable());

it('add user then update', function () {
    $userVoucherPivotTableName = config('vouchers.tables.user_voucher');

    $vh = prepareVoucherHelper();
    $vh->users = [$this->testUser];
    $voucher = VoucherManager::create($vh);

    assertDatabaseCount($userVoucherPivotTableName, 1);
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 1,
            'user_id' => $this->testUser->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );

    // update
    $voucher = $voucher->refresh();

    $vuh = new VoucherUpdateData($voucher);
    $user1 = TestUserFactory::new()->createOne();
    $vuh->users = [$user1];

    VoucherManager::update($vuh);

    assertTrue($vuh->isUsersDirty());

    assertDatabaseCount($userVoucherPivotTableName, 1);
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 2,
            'user_id' => $user1->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );
});

it('add user multiple the update', function () {
    $userVoucherPivotTableName = config('vouchers.tables.user_voucher');

    $anotherUser = TestUserFactory::new()->createOne();

    $vh = prepareVoucherHelper();
    $vh->users = [$this->testUser, $anotherUser];
    $voucher = VoucherManager::create($vh);

    assertDatabaseCount($userVoucherPivotTableName, 2);
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 1,
            'user_id' => $this->testUser->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 2,
            'user_id' => $anotherUser->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );

    // update
    $voucher = $voucher->refresh();

    $vuh = new VoucherUpdateData($voucher);
    $user1 = TestUserFactory::new()->createOne();
    $user2 = TestUserFactory::new()->createOne();
    $vuh->users = [$user1, $user2];

    VoucherManager::update($vuh);

    assertTrue($vuh->isUsersDirty());

    assertDatabaseCount($userVoucherPivotTableName, 2);
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 3,
            'user_id' => $user1->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 4,
            'user_id' => $user2->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );
});

it('check not update when dirty is false', function () {
    $userVoucherPivotTableName = config('vouchers.tables.user_voucher');
    $anotherUser = TestUserFactory::new()->createOne();

    $vh = prepareVoucherHelper();
    $vh->users = [$this->testUser, $anotherUser];
    $voucher = VoucherManager::create($vh);

    assertDatabaseCount($userVoucherPivotTableName, 2);
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 1,
            'user_id' => $this->testUser->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 2,
            'user_id' => $anotherUser->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );

    $vuh = new VoucherUpdateData($voucher);

    assertFalse($vuh->isUsersDirty());
    VoucherManager::update($vuh);

    assertDatabaseCount($userVoucherPivotTableName, 2);
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 1,
            'user_id' => $this->testUser->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );
    assertDatabaseHas(
        $userVoucherPivotTableName,
        [
            'id' => 2,
            'user_id' => $anotherUser->getKey(),
            'voucher_id' => $voucher->getKey(),
        ]
    );
});
