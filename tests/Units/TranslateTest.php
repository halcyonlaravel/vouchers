<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherFactory;

use function PHPUnit\Framework\assertEquals;

test('eligibility', function () {
    $voucher = VoucherFactory::new()->create(
        [
            'eligibility' => Voucher::ELIGIBILITY_EVERYONE,
        ]
    );
    assertEquals('Everyone', $voucher->eligibilityTranslated());

    $voucher = VoucherFactory::new()->create(
        [
            'eligibility' => Voucher::ELIGIBILITY_SPECIFIC_CUSTOMERS,
        ]
    );
    assertEquals('Specific Costumers', $voucher->eligibilityTranslated());

    $voucher = VoucherFactory::new()->create(
        [
            'eligibility' => Voucher::ELIGIBILITY_REGISTER_CUSTOMERS,
        ]
    );
    assertEquals('Registered Costumers', $voucher->eligibilityTranslated());
});
