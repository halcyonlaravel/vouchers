<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models\Limit;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestOrderFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestUserFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestUser;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

dataset('limit_data_dataset', [
    // basic
    // voucher, user
    [0, 0],
    [1, 1],
    [0, 1],
    [1, 0],

    [2, 0],
    [2, 3], // will throw exception
    [3, 2],
    [0, 2],
]);

it('saving', function () {
    $vh = prepareVoucherHelper();
    $vh->limit = 1;

    VoucherManager::create($vh);

    assertDatabaseHas(
        Voucher::class,
        [
            'limit_usage' => 1,
        ]
    );
});

it('add code in voucher limit table', function () {
    $model = VoucherFactory::new()->create(['limit_usage' => 10]);

    VoucherManager::useCode($this->testUser, $this->testOrder, $model->code);

    assertDatabaseHas(
        Voucher::class,
        [
            $model->getKeyName() => $model->getKey(),
            'limit_usage' => 10,
        ]
    );

    assertDatabaseHas(
        Limit::class,
        [
            'userable_type' => $this->testUser->getMorphClass(),
            'userable_id' => $this->testUser->getKey(),
            'orderable_type' => $this->testOrder->getMorphClass(),
            'orderable_id' => $this->testOrder->getKey(),
            'code' => $model->code,
            'used' => true,
        ]
    );
});

it('handle invalid id', function () {
    //        $this->expectException(ModelNotFoundException::class);
    VoucherManager::useCode($this->testUser, $this->testOrder, 'xxxxxxxxxxxxxxxxx');

    assertDatabaseCount(Limit::class, 0);
});

it('handle reach minimum usage', function () {
    //        $this->expectException(ValidationException::class);
    //        $this->expectErrorMessage('Maximum usage limit exceed.');

    $model = VoucherFactory::new()->create(['limit_usage' => 5]);

    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code); // 1
    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code); // 2
    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code); // 3
    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code); // 4
    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code); // 5
    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code); // this should not store

    assertDatabaseCount(Limit::class, 5);
});

it('success unlimited', function () {
    $model = VoucherFactory::new()->create(['limit_usage' => 0]);

    VoucherManager::useCode($this->testUser, $this->testOrder, $model->code);
    assertTrue(VoucherManager::limitUsageVoucherRule(null)->passes('', $model->code));

    VoucherManager::useCode($this->testGuest, $this->testOrder, $model->code);
    assertTrue(VoucherManager::limitUsageVoucherRule(null)->passes('', $model->code));

    // not exist
    assertFalse(VoucherManager::limitUsageVoucherRule(null)->passes('', 'xxxxxxxxxxxxxxx'));
});

it('valid usage limit', function () {
    $model = VoucherFactory::new()->create(['limit_usage' => 3]);

    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code);
    assertTrue(VoucherManager::limitUsageVoucherRule(null)->passes('', $model->code));

    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code);
    assertTrue(VoucherManager::limitUsageVoucherRule(null)->passes('', $model->code));

    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $model->code);
    assertFalse(VoucherManager::limitUsageVoucherRule(null)->passes('', $model->code));

    // not exist
    assertFalse(VoucherManager::limitUsageVoucherRule(null)->passes('', 'xxxxxxxxxxxxxxx'));
});

it('with limit user', function () {
    $user1 = TestUserFactory::new()->create();
    $user2 = TestUserFactory::new()->create();

    $model = VoucherFactory::new()->create(['limit_usage' => 5, 'limit_per_user' => 2]);

    $check = function (Voucher $model, TestUser $user) {
        assertTrue(
            VoucherManager::limitUsageVoucherRule($user)->passes('', $model->code)
        ); // limit 5, $user  2
        VoucherManager::useCode(
            $user,
            TestOrderFactory::new()->createOne(),
            $model->code
        );                                                                               // limit 4, $user  1

        assertTrue(
            VoucherManager::limitUsageVoucherRule($user)->passes('', $model->code)
        ); // limit 4, $user  1
        VoucherManager::useCode(
            $user,
            TestOrderFactory::new()->createOne(),
            $model->code
        );                                                                               // limit 3, $user  0

        assertfalse(
            VoucherManager::limitUsageVoucherRule($user)->passes('', $model->code)
        ); // limit 3, $user  0
    };

    $check($model, $user1);
    $check($model, $user2);

    assertTrue(VoucherManager::limitUsageVoucherRule($this->testGuest)->passes('', $model->code));
    VoucherManager::useCode($this->testGuest, TestOrderFactory::new()->createOne(), $model->code);

    assertFalse(VoucherManager::limitUsageVoucherRule($this->testGuest)->passes('', $model->code));

    assertEquals('Promo code not found.', VoucherManager::limitUsageVoucherRule(null)->message());
});

it('create limit use by voucher', function (int $limitPerVoucher, int $limitPerUser) {
    if ($limitPerVoucher !== 0 && $limitPerVoucher < $limitPerUser) {
        $this->expectExceptionMessage(
            'If `limit` is not `zero(0)`, the `limit` must grater than or equal in `limit_per_user`.'
        );
        $this->expectException(ValidationException::class);
    }

    $vh = prepareVoucherHelper();
    $vh->limit = $limitPerVoucher;
    $vh->limit_per_user = $limitPerUser;
    $voucher = VoucherManager::create($vh);

    if ($limitPerVoucher === 0) {
        foreach (range(1, 10) as $i) {
            assertTrue(VoucherManager::limitUsageVoucherRule($this->testGuest)->passes('', $voucher->code));
            VoucherManager::useCode($this->testGuest, TestOrderFactory::new()->createOne(), $voucher->code);
        }
    } else {
        foreach (range(1, $limitPerVoucher) as $i) {
            assertTrue(VoucherManager::limitUsageVoucherRule($this->testGuest)->passes('', $voucher->code));
            VoucherManager::useCode($this->testGuest, TestOrderFactory::new()->createOne(), $voucher->code);
        }

        assertFalse(VoucherManager::limitUsageVoucherRule($this->testGuest)->passes('', $voucher->code));
    }
})
    ->with('limit_data_dataset');

it('create limit use by user', function (int $limitPerVoucher, int $limitPerUser) {
    if ($limitPerVoucher !== 0 && $limitPerVoucher < $limitPerUser) {
        $this->expectExceptionMessage(
            'If `limit` is not `zero(0)`, the `limit` must grater than or equal in `limit_per_user`.'
        );
        $this->expectException(ValidationException::class);
    }

    $vh = prepareVoucherHelper();
    $vh->limit = $limitPerVoucher;
    $vh->limit_per_user = $limitPerUser;
    $voucher = VoucherManager::create($vh);

    if ($limitPerUser === 0) {
        foreach (range(1, $limitPerVoucher) as $i) {
            assertTrue(VoucherManager::limitUsageVoucherRule($this->testUser)->passes('', $voucher->code));
            VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
        }
        if ($limitPerVoucher !== 0) {
            foreach (range(1, 10) as $i) {
                assertFalse(
                    VoucherManager::limitUsageVoucherRule($this->testUser)->passes('', $voucher->code)
                );
            }
        }
    } else {
        foreach (range(1, $limitPerUser) as $i) {
            assertTrue(VoucherManager::limitUsageVoucherRule($this->testUser)->passes('', $voucher->code));
            VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
        }

        assertFalse(VoucherManager::limitUsageVoucherRule($this->testUser)->passes('', $voucher->code));
    }
})
    ->with('limit_data_dataset');
