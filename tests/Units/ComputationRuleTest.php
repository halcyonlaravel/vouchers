<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestCanMultipleBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function PHPUnit\Framework\assertEmpty;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertNotEmpty;
use function PHPUnit\Framework\assertTrue;

test('validate can not multiple', function () {
    $voucher = VoucherManager::create(prepareVoucherHelper());

    assertFalse($voucher->computation->class->rules()->canMultipleUse);

    assertNotEmpty(VoucherManager::validateComputations([$voucher, $voucher]));
});

test('validate can multiple', function () {
    $vh = prepareVoucherHelper();
    $vh->computation = (new ComputationData(TestCanMultipleBasicBaseVoucherComputation::class))->setValue(
        'test'
    );
    $voucher = VoucherManager::create($vh);

    assertTrue($voucher->computation->class->rules()->canMultipleUse);

    assertEmpty(VoucherManager::validateComputations([$voucher, $voucher]));
});
