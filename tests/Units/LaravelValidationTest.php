<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Helper;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

it('valid class', function () {
    $v = VoucherManager::validForVoucherRule(Helper::TYPE_REQUIREMENTS);
    assertTrue($v->passes('', TestBasicVoucherRequirements::name()));
    assertFalse($v->passes('', TestBasicBaseVoucherComputation::name()));
    assertFalse($v->passes('', 'xx'));

    $v = VoucherManager::validForVoucherRule(Helper::TYPE_COMPUTATIONS);
    assertTrue($v->passes('', TestBasicBaseVoucherComputation::name()));
    assertFalse($v->passes('', TestBasicVoucherRequirements::name()));
    assertFalse($v->passes('', 'xx'));

    assertEquals('Promo code not found.', $v->message());
});

it('valid eligibility', function () {
    $v = VoucherManager::validEligibilityVoucherRule();
    assertTrue($v->passes('', Voucher::ELIGIBILITY_EVERYONE));
    assertTrue($v->passes('', Voucher::ELIGIBILITY_REGISTER_CUSTOMERS));
    assertTrue($v->passes('', Voucher::ELIGIBILITY_SPECIFIC_CUSTOMERS));
    assertFalse($v->passes('', 'xx'));

    assertEquals('Promo code not found.', $v->message());
});

it('unique title bulk', function () {
    assertAllEmptyVoucherTable();
    // Other
    $vh = prepareVoucherHelper();
    $vh->title = 'other title';
    VoucherManager::create($vh, null, 5);

    $vh = prepareVoucherHelper();
    $vh->title = 'my title';
    $vouchers = VoucherManager::create($vh, null, 5);

    assertDatabaseCount(Voucher::class, 10);

    // update
    $v = VoucherManager::uniqueByBatchRule()->ignore($vouchers[0]);

    assertTrue($v->passes('', $vouchers[0]->title));
    assertTrue($v->passes('', $vouchers[0]->title.' new'));
    assertFalse($v->passes('', 'other title'));

    // create
    $v = VoucherManager::uniqueByBatchRule();

    assertFalse($v->passes('', $vouchers[0]->title));
    assertTrue($v->passes('', $vouchers[0]->title.' new'));
    assertFalse($v->passes('', 'other title'));

    assertEquals('The :attribute already in use.', $v->message());
});

it('unique title single', function () {
    assertAllEmptyVoucherTable();
    // Other
    $vh = prepareVoucherHelper();
    $vh->title = 'other title';
    VoucherManager::create($vh, null, 5);

    $vh = prepareVoucherHelper();
    $vh->title = 'my title';
    $voucher = VoucherManager::create($vh);

    assertDatabaseCount(Voucher::class, 6);

    // update
    $v = VoucherManager::uniqueByBatchRule()->ignore($voucher);

    assertTrue($v->passes('', $voucher->title));
    assertTrue($v->passes('', $voucher->title.' new'));
    assertFalse($v->passes('', 'other title'));

    // create
    $v = VoucherManager::uniqueByBatchRule();

    assertFalse($v->passes('', $voucher->title));
    assertTrue($v->passes('', $voucher->title.' new'));
    assertFalse($v->passes('', 'other title'));
});
