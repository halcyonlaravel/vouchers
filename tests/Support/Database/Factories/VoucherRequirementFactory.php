<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use Illuminate\Database\Eloquent\Factories\Factory;

class VoucherRequirementFactory extends Factory
{
    protected $model = Requirement::class;

    public function definition(): array
    {
        return [
            'voucher_id' => VoucherFactory::new(),
            'class' => TestBasicVoucherRequirements::class,
        ];
    }
}
