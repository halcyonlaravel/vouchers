<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Models\Limit;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestOrder;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class VoucherLimitFactory extends Factory
{
    protected $model = Limit::class;

    public function definition(): array
    {
        return [
            'userable_type' => (new TestUser())->getTable(), //Helper::getMorphedModel(TestUser::class),
            'userable_id' => 1,
            'orderable_type' => (new TestOrder())->getTable(), //Helper::getMorphedModel(TestOrder::class),
            'orderable_id' => 1,
        ];
    }
}
