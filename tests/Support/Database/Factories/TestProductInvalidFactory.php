<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestProductInvalid;
use Illuminate\Database\Eloquent\Factories\Factory;

class TestProductInvalidFactory extends Factory
{
    protected $model = TestProductInvalid::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->firstName,
        ];
    }
}
