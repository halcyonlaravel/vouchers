<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use Illuminate\Database\Eloquent\Factories\Factory;

class VoucherFactory extends Factory
{
    protected $model = Voucher::class;

    public function definition(): array
    {
        return [
            'code' => $this->faker->unique()->word,
            'batch' => 1,
            'limit_usage' => 1,
            'limit_per_user' => 0,
            'valid_start_at' => now(),
            'eligibility' => Voucher::ELIGIBILITY_EVERYONE,
        ];
    }
}
