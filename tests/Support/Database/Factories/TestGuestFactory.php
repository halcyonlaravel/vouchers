<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestGuest;
use Illuminate\Database\Eloquent\Factories\Factory;

class TestGuestFactory extends Factory
{
    protected $model = TestGuest::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->firstName,
        ];
    }
}
