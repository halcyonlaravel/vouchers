<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestOrder;
use Illuminate\Database\Eloquent\Factories\Factory;

class TestOrderFactory extends Factory
{
    protected $model = TestOrder::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->firstName,
        ];
    }
}
