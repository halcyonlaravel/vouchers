<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Models\Computation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use Illuminate\Database\Eloquent\Factories\Factory;

class VoucherComputationFactory extends Factory
{
    protected $model = Computation::class;

    public function definition(): array
    {
        return [
            'voucher_id' => VoucherFactory::new(),
            'class' => TestBasicBaseVoucherComputation::class,
        ];
    }
}
