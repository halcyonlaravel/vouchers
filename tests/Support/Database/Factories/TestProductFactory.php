<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

class TestProductFactory extends Factory
{
    protected $model = TestProduct::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->firstName,
        ];
    }
}
