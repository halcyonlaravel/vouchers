<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories;

use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class TestUserFactory extends Factory
{
    protected $model = TestUser::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->firstName,
        ];
    }
}
