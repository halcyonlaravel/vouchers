<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation;

use HalcyonLaravelBoilerplate\Vouchers\Formulas\BaseVoucherComputation;
use Money\Money;

class TestBasicBaseVoucherComputation extends BaseVoucherComputation
{
    public static function name(): string
    {
        return 'test_basic_computation_formula';
    }

    public function discountPrice(Money $price = null): Money
    {
        return Money::PHP($this->class->value);
    }

    public static function type(): string
    {
        return __METHOD__;
    }
}
