<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation;

use HalcyonLaravelBoilerplate\Vouchers\Formulas\BaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Formulas\ComputationRule;
use Money\Money;

class TestCanMultipleBasicBaseVoucherComputation extends BaseVoucherComputation
{
    public static function name(): string
    {
        return 'test_basic_computation_formula_x';
    }

    public function discountPrice(Money $price = null): Money
    {
        return Money::PHP(0);
    }

    public function rules(): ComputationRule
    {
        $r = new ComputationRule();
        $r->canMultipleUse = true;

        return $r;
    }

    public static function type(): string
    {
        return __METHOD__;
    }
}
