<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements;

use HalcyonLaravelBoilerplate\Vouchers\Formulas\BaseVoucherRequirement;

class TestBasicVoucherRequirements extends BaseVoucherRequirement
{
    public static function name(): string
    {
        return 'test_basic_rule_formula';
    }

    public function validate(): bool
    {
        //        dd(__METHOD__,$this->voucherModel);
        //        dd($this->class->value);
        return $this->class->value === 'ok';
    }
}
