<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements;

use HalcyonLaravelBoilerplate\Vouchers\Formulas\BaseVoucherRequirement;

class TestBasicOtherVoucherRequirements extends BaseVoucherRequirement
{
    public static function name(): string
    {
        return 'test_basic_rule_formula_other';
    }

    public function validate(): bool
    {
        //        dd(__METHOD__,$this->voucherModel);
        return $this->class->value;
    }
}
