<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\UserableLimitVoucherContract;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @mixin \Eloquent
 */
class TestUser extends Authenticatable implements UserableLimitVoucherContract
{
    protected $guarded = [];

    protected $table = 'test_users';

    public function voucherLimitTitle(): string
    {
        return __METHOD__;
    }
}
