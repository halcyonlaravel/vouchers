<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\Override;

use HalcyonLaravelBoilerplate\Vouchers\Models\Computation;

class TestComputation extends Computation
{
}
