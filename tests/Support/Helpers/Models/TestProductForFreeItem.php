<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\HasVoucherComputationFreeItem;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 *
 * @property string name
 */
class TestProductForFreeItem extends Model implements HasVoucherComputationFreeItem
{
    protected $guarded = [];

    protected $table = 'test_product_for_free_items';

    public function voucherFreeItemTitle(): string
    {
        return $this->name;
    }
}
