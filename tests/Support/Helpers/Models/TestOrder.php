<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\OrderableLimitVoucherContract;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class TestOrder extends Model implements OrderableLimitVoucherContract
{
    protected $guarded = [];

    protected $table = 'test_orders';

    public function voucherLimitTitle(): string
    {
        return __METHOD__;
    }
}
