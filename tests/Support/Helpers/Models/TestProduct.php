<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models;

use HalcyonLaravelBoilerplate\Vouchers\Contracts\HasVoucherComputationFreeItem;
use HalcyonLaravelBoilerplate\Vouchers\Traits\HasVoucherTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 *
 * @property string name
 */
class TestProduct extends Model implements HasVoucherComputationFreeItem
{
    use HasVoucherTrait;

    protected $guarded = [];

    protected $table = 'test_products';

    public function voucherFreeItemTitle(): string
    {
        return $this->name;
    }

    public function voucherTitle(): string
    {
        return $this->name;
    }
}
