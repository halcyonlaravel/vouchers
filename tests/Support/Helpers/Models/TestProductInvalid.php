<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class TestProductInvalid extends Model
{
    protected $guarded = [];

    protected $table = 'test_product_invalid';
}
