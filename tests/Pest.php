<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Tests\TestCase;
use Illuminate\Database\Eloquent\Model;

uses(TestCase::class)
    ->beforeEach(function () {
        Model::preventLazyLoading();
    })
    ->in(__DIR__);
