<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\CodeGeneratorData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Facades\VouchersFacade;
use HalcyonLaravelBoilerplate\Vouchers\Models\Computation;
use HalcyonLaravelBoilerplate\Vouchers\Models\ComputationModel;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertMatchesRegularExpression;
use function PHPUnit\Framework\assertTrue;

it('create single with code', function () {
    $item = new VoucherData();
    $item->limit_per_user = 0;
    //        $item->RequirementDatas = [TestBasicVoucherRequirements::name()];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue(
        'test value'
    );

    $item->eligibility = Voucher::ELIGIBILITY_REGISTER_CUSTOMERS;

    $start = now();

    $item->valid_start_at = $start;
    $item->title = 'Title';
    $item->code = 'Code';
    $item->description = 'description';

    $item->remarks = 'test remarks';

    VouchersFacade::create($item);

    assertDatabaseHas(
        Voucher::class,
        [
            'type' => 'type',
            'title' => 'Title',
            'code' => 'Code',
            'description' => 'description',
            'eligibility' => Voucher::ELIGIBILITY_REGISTER_CUSTOMERS,
            'limit_usage' => 0,
            'limit_per_user' => 0,
            'valid_start_at' => $start,
            'valid_end_at' => null,
            //                'requirements' => json_encode(['test_basic_rule_formula']),
            'remarks' => 'test remarks',
        ]
    );

    assertDatabaseHas(
        Computation::class,
        [
            'class' => TestBasicBaseVoucherComputation::name(),
            'value' => 'test value',
        ]
    );
});

it('create single with generated code', function () {
    $item = new VoucherData();
    $item->limit = 0;
    $item->limit_per_user = 0;
    //        $item->RequirementDatas = [TestBasicVoucherRequirements::name()];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue(
        'test value'
    );
    $item->valid_start_at = now();
    $item->title = 'Title';
    $item->description = 'description';

    $CodeGeneratorData = new CodeGeneratorData();
    $CodeGeneratorData->characters = '1234567890';
    $code = VouchersFacade::create($item, $CodeGeneratorData)->code;

    assertMatchesRegularExpression('/^[0-9]/', $code);
});

it('create multiple with generated code', function () {
    $VoucherData = new VoucherData();
    $VoucherData->limit = 0;
    $VoucherData->limit_per_user = 0;
    //        $VoucherData->RequirementDatas = [new RequirementData(TestBasicVoucherRequirements::name(), 'test')];
    $VoucherData->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue(
        'test'
    );
    $VoucherData->valid_start_at = now();

    $CodeGeneratorData = new CodeGeneratorData();
    $CodeGeneratorData->mask = '****&****';
    $CodeGeneratorData->characters = '1234567890';
    $CodeGeneratorData->separator = '&';
    $CodeGeneratorData->prefix = 'prefix';
    $CodeGeneratorData->suffix = 'suffix';

    $vouchers = VouchersFacade::create($VoucherData, $CodeGeneratorData, 3);

    $regex = '/^prefix&(\d){4}&(\d){4}&suffix$/';

    assertMatchesRegularExpression($regex, $vouchers[0]->code);
    assertMatchesRegularExpression($regex, $vouchers[1]->code);
    assertMatchesRegularExpression($regex, $vouchers[2]->code);

    assertDatabaseCount(Voucher::class, 3);
});

it('unique', function () {
    $chars = str_split('QWERTYUIOP');

    foreach ($chars as $char) {
        VoucherFactory::new()->create(
            [
                'code' => $char,
            ]
        );
    }

    $item = new VoucherData();
    $item->limit = 0;
    $item->limit_per_user = 0;
    //        $item->RequirementDatas = [TestBasicVoucherRequirements::name()];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->valid_start_at = now();

    $testChars = 'QWERTY23456';
    $CodeGeneratorData = new CodeGeneratorData();
    $CodeGeneratorData->mask = '*';
    $CodeGeneratorData->characters = $testChars;

    $generateCount = 5;
    $vouchers = VouchersFacade::create($item, $CodeGeneratorData, $generateCount);

    assertCount($generateCount, $vouchers);

    assertDatabaseCount(Voucher::class, $generateCount + count($chars));
});

//    /** @test */
//    public function lowercase()
//    {
//        $item = new VoucherData();
//        $item->requirements = [TestBasicVoucherRequirements::name()];
//        $item->computation = new TestBasicVoucherComputation();
//        $item->valid_start_at = now();
//
//        $CodeGeneratorData = new CodeGeneratorData();
//        $CodeGeneratorData->characters = 'ABCDEFG';
//        $CodeGeneratorData->case = CodeGeneratorData::CASE_LOWER;
//        $code = VouchersFacade::create($item, $CodeGeneratorData)->code;
//
//        $this->assertMatchesRegularExpression('/^[a-z]{4}-[a-z]{4}$/', $code);
//    }

test('with class name only', function () {
    $item = new VoucherData();
    $item->limit = 0;
    $item->limit_per_user = 0;
    $item->requirements = [new RequirementData(TestBasicVoucherRequirements::name(), 'x')];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::name()))->setValue('test');
    $item->valid_start_at = now();

    VouchersFacade::create($item);

    assertTrue(true);
});

test('create with computation with models', function () {
    $item = new VoucherData();
    $item->limit = 0;
    $item->limit_per_user = 0;
    $item->requirements = [new RequirementData(TestBasicVoucherRequirements::name(), 'test')];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::name()))->setModels(
        [$this->testProduct]
    );
    $item->valid_start_at = now();

    $voucher = VouchersFacade::create($item);

    assertDatabaseHas(
        ComputationModel::class,
        [
            'computation_id' => $voucher->computation->getKey(),
            'model_type' => $this->testProduct->getMorphClass(),
            'model_id' => $this->testProduct->getKey(),
        ]
    );
    assertDatabaseCount(ComputationModel::class, 1);
});

it('remove special characters in voucher code', function () {
    $specialCharacters = '!@#$%^&*()+={}:"<>?[];\',./';
    $validCode = 'sample1234567890_-';

    $item = prepareVoucherHelper();
    $item->code = $validCode.$specialCharacters;
    $voucher = VouchersFacade::create($item);

    assertEquals($validCode, $voucher->code);

    $vuh = new VoucherUpdateData($voucher);
    $vuh->code = $specialCharacters.$validCode;
    VoucherManager::update($vuh);

    assertEquals($validCode, $voucher->refresh()->code);
});

it('throw exception when code is empty after removing special character', function () {
    $specialCharacters = '!@#$%^&*()+={}:"<>?[];\',./';

    $vh = prepareVoucherHelper();
    $vh->code = $specialCharacters;

    $vh->validate();
})
    ->throws(ValidationException::class, 'Code is empty after removing spacial characters');
