<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\CodeGeneratorData;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

beforeEach(fn () => assertAllEmptyVoucherTable());

it('is bulk', function () {
    VoucherManager::create(prepareVoucherHelper(), null, 3);

    assertTrue(Voucher::first()->isBulk());
});

it('is not bulk', function () {
    VoucherManager::create(prepareVoucherHelper());

    assertFalse(Voucher::first()->isBulk());
});

it(
    'get prefix for bulk',
    function () {
        $gh = new CodeGeneratorData();
        $gh->prefix = 'test-prefix';
        VoucherManager::create(prepareVoucherHelper(), $gh, 3);

        assertEquals('test-prefix-', Voucher::first()->getPrefix());
    }
);

it('get prefix for none bulk', function () {
    $vh = prepareVoucherHelper();
    $vh->code = 'test code 123';
    VoucherManager::create($vh);

    assertEquals('test_code_123', Voucher::first()->getPrefix());
});
