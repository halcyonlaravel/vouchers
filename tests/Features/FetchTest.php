<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertTrue;

test('bulk', function () {
    assertAllEmptyVoucherTable();

    $vh = prepareVoucherHelper();
    $vh->title = 'test bulk title 1';
    VoucherManager::create($vh, null, 10);

    $vh = prepareVoucherHelper();
    $vh->title = 'test bulk title 2';
    VoucherManager::create($vh, null, 10);

    $vh = prepareVoucherHelper();
    $vh->title = 'test bulk title 3';
    VoucherManager::create($vh, null, 10);

    assertDatabaseCount(Voucher::class, 30);
    assertCount(3, VoucherManager::all());

    $data = VoucherManager::all();

    assertTrue(isset($data[0]['batch']));
    assertTrue(isset($data[0]['title']));
    assertTrue(isset($data[0]['created_at']));
});
