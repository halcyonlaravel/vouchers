<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;

test('single', function () {
    VoucherManager::create(prepareVoucherHelper());

    assertDatabaseHas(
        Voucher::class,
        [
            'batch' => 1,
        ]
    );

    assertDatabaseCount((new Voucher())->getTable(), 1);
});

test('custom', function () {
    $vh = prepareVoucherHelper();
    $vh->batch = 123;
    VoucherManager::create($vh);

    assertDatabaseHas(
        Voucher::class,
        [
            'batch' => 123,
        ]
    );

    VoucherManager::create(prepareVoucherHelper());
    assertDatabaseHas(
        Voucher::class,
        [
            'batch' => 124,
        ]
    );

    assertDatabaseCount(Voucher::class, 2);
});

test('bulk', function () {
    VoucherManager::create(prepareVoucherHelper(), null, 10);

    assertDatabaseHas(
        Voucher::class,
        [
            'batch' => 1,
        ]
    );
    assertDatabaseMissing(
        Voucher::class,
        [
            'batch' => 2,
        ]
    );

    assertDatabaseCount(Voucher::class, 10);

    VoucherManager::create(prepareVoucherHelper(), null, 10);

    assertDatabaseHas(
        Voucher::class,
        [
            'batch' => 1,
        ]
    );
    assertDatabaseHas(
        Voucher::class,
        [
            'batch' => 2,
        ]
    );

    assertDatabaseCount(Voucher::class, 20);
});
