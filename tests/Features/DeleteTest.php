<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;
use HalcyonLaravelBoilerplate\Vouchers\Models\Computation;
use HalcyonLaravelBoilerplate\Vouchers\Models\ComputationModel;
use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestProduct;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;
use Symfony\Component\HttpKernel\Exception\HttpException;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseMissing;

beforeEach(fn () => assertAllEmptyVoucherTable());

dataset('data_resource_dataset', [
    // isModels, hasReq
    [true, true],
    [false, false],
    [true, false],
    [false, true],
]);

test('delete bulk', function (bool $isModels, bool $hasReq) {
    bulkTest($isModels, $hasReq);
})->with('data_resource_dataset');

test('delete single', function (bool $isModels, bool $hasReq) {
    singleTest($isModels, $hasReq);
})->with('data_resource_dataset');

test('delete by model instance', function () {
    // others
    VoucherManager::create(xprepareVoucherHelper(true, true), null, 4);

    $voucher = VoucherManager::create(xprepareVoucherHelper(true, true));
    assertVoucherTable(5, 5, 5);
    $voucher->delete();

    assertDatabaseMissing(
        Voucher::class,
        [
            'code' => $voucher->code,
        ]
    );

    assertVoucherTable(4, 4, 4);
});

test('delete by model instance invalid when used', function () {
    // others
    VoucherManager::create(xprepareVoucherHelper(true, true), null, 4);

    $voucher = VoucherManager::create(xprepareVoucherHelper(true, true));
    assertVoucherTable(5, 5, 5);
    VoucherManager::useCode($this->testUser, $this->testOrder, $voucher->code);
    $voucher->delete();
})
    ->throws(HttpException::class, 'Cannot delete promo, already in use.');

test('throw exception when already in used', function () {
    //        $this->expectExceptionCode(422);

    $voucher = VoucherManager::create(xprepareVoucherHelper());

    VoucherManager::useCode($this->testUser, $this->testOrder, $voucher->code);

    VoucherManager::delete($voucher->code);
})
    ->throws(HttpException::class, 'Cannot delete promo, already in use.');

test('throw exception when already in use bulk', function () {
    //        $this->expectExceptionCode(422);

    $vouchers = VoucherManager::create(xprepareVoucherHelper(), null, 3);

    $voucher = $vouchers[count($vouchers) - 1];
    VoucherManager::useCode($this->testUser, $this->testOrder, $voucher->code);

    VoucherManager::delete($voucher->code);
})
    ->throws(HttpException::class, 'Cannot delete promo, already in use.');

/**
 * @throws \Throwable
 */
function bulkTest(bool $isModels, bool $hasReq)
{
    // others
    VoucherManager::create(xprepareVoucherHelper($isModels, $hasReq), null, 4);

    $vouchers = VoucherManager::create(xprepareVoucherHelper($isModels, $hasReq), null, 4);

    VoucherManager::delete($vouchers[0]->code, true);

    foreach ($vouchers as $voucher) {
        assertDatabaseMissing(
            Voucher::class,
            [
                'code' => $voucher->code,
            ]
        );
    }

    assertVoucherTable(4, $isModels ? 4 : 0, $hasReq ? 4 : 0);
}

/**
 * @throws \Throwable
 */
function singleTest(bool $isModels, bool $hasReq)
{
    // others
    VoucherManager::create(xprepareVoucherHelper($isModels, $hasReq), null, 4);

    $voucher = VoucherManager::create(xprepareVoucherHelper($isModels, $hasReq));

    VoucherManager::delete($voucher->code);

    assertDatabaseMissing(
        Voucher::class,
        [
            'code' => $voucher->code,
        ]
    );

    assertVoucherTable(4, $isModels ? 4 : 0, $hasReq ? 4 : 0);
}

function assertVoucherTable(int $count, int $computationModelCount, int $requirementCount)
{
    assertDatabaseCount(Voucher::class, $count);
    assertDatabaseCount(Requirement::class, $requirementCount);
    assertDatabaseCount(Computation::class, $count);
    assertDatabaseCount(ComputationModel::class, $computationModelCount);
}

function xprepareVoucherHelper(bool $isModels = true, bool $hasReq = true): VoucherData
{
    $vh = prepareVoucherHelper();
    if ($isModels) {
        $vh->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))
            ->setModels([TestProduct::first()]);
        if ($hasReq) {
            $vh->requirements = [
                new RequirementData(TestBasicVoucherRequirements::class, TestProduct::first()),
            ];
        }
    } else {
        $vh->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))
            ->setValue('test');
        if ($hasReq) {
            $vh->requirements = [
                new RequirementData(TestBasicVoucherRequirements::class, 'test'),
            ];
        }
    }

    return $vh;
}
