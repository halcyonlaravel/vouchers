<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\CodeGeneratorData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Facades\VouchersFacade;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;

test('exceed unique', function () {
    VoucherFactory::new()->create(
        [
            'code' => 'Q',
        ]
    );
    VoucherFactory::new()->create(
        [
            'code' => 'W',
        ]
    );
    VoucherFactory::new()->create(
        [
            'code' => 'E',
        ]
    );

    $item = new VoucherData();
    $item->limit = 0;
    $item->limit_per_user = 0;
    //        $item->requirementHelpers = [TestBasicVoucherRequirements::name()];
    $item->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $item->valid_start_at = now();

    $testChars = 'Q';
    $CodeGeneratorData = new CodeGeneratorData();
    $CodeGeneratorData->mask = '*';
    $CodeGeneratorData->characters = $testChars;

    VouchersFacade::create($item, $CodeGeneratorData);
})
    ->throws(ValidationException::class, 'Unique code `Q` exceed in mask `*` at `4` times.');

test('amount below 1', function () {
    $item = new VoucherData();
    $item->requirements = [TestBasicVoucherRequirements::name()];
    //        $item->computation = TestBasicBaseVoucherComputation::class;
    $item->valid_start_at = now();

    $g = new CodeGeneratorData();

    VouchersFacade::create($item, $g, 0);
})
    ->throws(ValidationException::class, 'Minimum amount generate must not below 1, 0 given.');
