<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;
use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestUserFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;

beforeEach(fn () => assertAllEmptyVoucherTable());

it('create empty requirements', function () {
    $vh = prepareVoucherHelper();
    $vh->requirements = [];

    VoucherManager::create($vh);

    assertDatabaseCount(Requirement::class, 0);
});

it('remove requirements', function () {
    $vh = prepareVoucherHelper();
    $vh->requirements = [
        new RequirementData(TestBasicVoucherRequirements::name(), 'test'),
        new RequirementData(TestBasicVoucherRequirements::name(), $this->testProduct),
    ];

    $voucher = VoucherManager::create($vh);

    assertDatabaseCount(Requirement::class, 2);

    $vuh = new VoucherUpdateData($voucher);
    $vuh->requirements = [];

    VoucherManager::update($vuh);

    assertDatabaseCount(Requirement::class, 0);
});

it('create empty users', function () {
    $vh = prepareVoucherHelper();
    $vh->users = [];

    VoucherManager::create($vh);

    assertDatabaseCount(config('vouchers.tables.user_voucher'), 0);
});

it('removes_users', function () {
    $vh = prepareVoucherHelper();
    $vh->users = [
        TestUserFactory::new()->createOne(),
        TestUserFactory::new()->createOne(),
    ];

    $voucher = VoucherManager::create($vh);

    assertDatabaseCount(config('vouchers.tables.user_voucher'), 2);

    $vuh = new VoucherUpdateData($voucher);
    $vuh->users = [];
    VoucherManager::update($vuh);

    assertDatabaseCount(config('vouchers.tables.user_voucher'), 0);
});
