<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherUpdateData;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models\Computation;
use HalcyonLaravelBoilerplate\Vouchers\Models\ComputationModel;
use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestProductFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestCanMultipleBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicOtherVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

beforeEach(fn () => assertAllEmptyVoucherTable());

it('single update success', function () {
    $voucher = prepareVoucherHelperWithComputationAndRequirement(1);

    assertEquals('old title', $voucher->title);

    // update

    $compProductModel1 = TestProductFactory::new()->createOne();
    $compProductModel2 = TestProductFactory::new()->createOne();
    $vuh = new VoucherUpdateData($voucher);
    $vuh->title = 'new title';
    $vuh->computation = (new ComputationData(TestCanMultipleBasicBaseVoucherComputation::name()))
        ->setModels([$compProductModel1, $compProductModel2]);

    $reqProductModel = TestProductFactory::new()->createOne();
    $vuh->requirements = [
        new RequirementData(TestBasicOtherVoucherRequirements::class, 'test2'),
        new RequirementData(TestBasicOtherVoucherRequirements::class, $reqProductModel),
    ];

    VoucherManager::update($vuh);

    assertTrue($vuh->isComputationDirty());
    assertTrue($vuh->isRequirementsDirty());

    $voucher = $voucher->refresh();

    assertEquals('new title', $voucher->title);
    assertDatabaseCount(Requirement::class, 2);
    assertDatabaseCount(Computation::class, 1);
    assertDatabaseCount(ComputationModel::class, 2);

    assertDatabaseMissing(Computation::class, ['id' => 2]);
    assertDatabaseHas(Computation::class, ['id' => 1]);

    foreach ([1, 2, 3] as $id) {
        assertDatabaseMissing(ComputationModel::class, ['id' => $id]);
    }
    foreach ([4, 5] as $id) {
        assertDatabaseHas(ComputationModel::class, ['id' => $id]);
    }

    foreach ([1, 2, 3] as $id) {
        assertDatabaseMissing(Requirement::class, ['id' => $id]);
    }
    foreach ([4, 5] as $id) {
        assertDatabaseHas(Requirement::class, ['id' => $id]);
    }
});

it('bulk update success', function () {
    prepareVoucherHelperWithComputationAndRequirement(3);

    foreach (VoucherManager::all() as $voucher) {
        assertEquals('old title', $voucher->title);
    }

    assertDatabaseCount(Requirement::class, 9);
    assertDatabaseCount(ComputationModel::class, 9);

    // update
    $vuh = new VoucherUpdateData(VoucherManager::all()->first());

    $vuh->title = 'new title';

    $compProductModel1 = TestProductFactory::new()->createOne();
    $compProductModel2 = TestProductFactory::new()->createOne();
    $vuh->computation = (new ComputationData(TestCanMultipleBasicBaseVoucherComputation::name()))
        ->setModels([$compProductModel1, $compProductModel2]);

    $reqProductModel = TestProductFactory::new()->createOne();
    $vuh->requirements = [
        new RequirementData(TestBasicOtherVoucherRequirements::class, 'test2'),
        new RequirementData(TestBasicOtherVoucherRequirements::class, $reqProductModel),
    ];

    VoucherManager::update($vuh);

    assertTrue($vuh->isComputationDirty());
    assertTrue($vuh->isRequirementsDirty());

    foreach (VoucherManager::all() as $voucher) {
        assertEquals('new title', $voucher->title);
    }
    assertDatabaseCount(Requirement::class, 6);
    assertDatabaseCount(ComputationModel::class, 6);
});

it('code must be null if bulk created', function () {
    VoucherManager::create(prepareVoucherHelper(), null, 3);

    $vuh = new VoucherUpdateData(Voucher::first());
    $vuh->code = 'new code test';

    VoucherManager::update($vuh);
})
    ->throws(ValidationException::class, 'Code must be null if voucher is bulk created.');

it('dont recreate data when dirty is false', function () {
    $voucher = prepareVoucherHelperWithComputationAndRequirement(1);

    $vuh = new VoucherUpdateData($voucher);

    VoucherManager::update($vuh);

    assertFalse($vuh->isComputationDirty());
    assertFalse($vuh->isRequirementsDirty());

    assertDatabaseCount(Requirement::class, 3);
    assertDatabaseCount(Computation::class, 1);
    assertDatabaseCount(ComputationModel::class, 3);

    assertDatabaseMissing(Computation::class, ['id' => 2]);
    assertDatabaseHas(Computation::class, ['id' => 1]);

    foreach ([1, 2, 3] as $id) {
        assertDatabaseHas(ComputationModel::class, ['id' => $id]);
    }
    foreach ([4, 5, 6] as $id) {
        assertDatabaseMissing(ComputationModel::class, ['id' => $id]);
    }

    foreach ([1, 2, 3] as $id) {
        assertDatabaseHas(Requirement::class, ['id' => $id]);
    }
    foreach ([4, 5, 6] as $id) {
        assertDatabaseMissing(Requirement::class, ['id' => $id]);
    }
});

function prepareVoucherHelperWithComputationAndRequirement(int $amount)
{
    $vh = test()->prepareVoucherHelper();

    $vh->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))
        ->setValue('test')
        ->setModels(
            [
                TestProductFactory::new()->createOne(),
                TestProductFactory::new()->createOne(),
                TestProductFactory::new()->createOne(),
            ]
        );

    $vh->requirements = [
        new RequirementData(TestBasicVoucherRequirements::class, 'test'),
        new RequirementData(TestBasicVoucherRequirements::name(), test()->testProduct),
        new RequirementData(TestBasicVoucherRequirements::name(), test()->testProduct),
    ];

    $vh->title = 'old title';

    $voucher = VoucherManager::create($vh, null, $amount);

    assertDatabaseCount(Requirement::class, $amount * 3);
    assertDatabaseCount(Computation::class, $amount * 1);
    assertDatabaseCount(ComputationModel::class, $amount * 3);

    return $voucher;
}
