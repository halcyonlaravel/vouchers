<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\Helper;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function PHPUnit\Framework\assertSame;

test(
    'for select',
    function () {
        assertSame(
            [
                'test_basic_computation_formula' => 'Test Basic Computation',
                'test_basic_computation_formula_x' => 'vouchers::label.computations.test_basic_computation_formula_x',
            ],
            VoucherManager::forSelect(Helper::TYPE_COMPUTATIONS)
        );
        assertSame(
            [
                'test_basic_rule_formula' => 'Test Basic Rule',
            ],
            VoucherManager::forSelect(Helper::TYPE_REQUIREMENTS)
        );
    }
);
