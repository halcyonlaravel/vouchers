<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;
use HalcyonLaravelBoilerplate\Vouchers\Exceptions\ValidationException;
use HalcyonLaravelBoilerplate\Vouchers\Models\Requirement;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseHas;

it('assign in single product', function () {
    $vh = prepareVoucherHelper();

    $vh->requirements = [
        new RequirementData(TestBasicVoucherRequirements::class, $this->testProduct),
    ];

    $model = VoucherManager::create($vh);

    assertDatabaseHas(
        (new Requirement())->getTable(),
        [
            'voucher_id' => $model->getKey(),
            'model_type' => $this->testProduct->getMorphClass(),
            'model_id' => $this->testProduct->getKey(),
            'class' => TestBasicVoucherRequirements::name(),
        ]
    );
});

it('invalid model', function () {
    $vh = prepareVoucherHelper();

    $vh->requirements = [
        new RequirementData(TestBasicVoucherRequirements::name(), $this->testProductInvalid),
    ];

    VoucherManager::create($vh);
})
    ->throws(
        ValidationException::class,
        'Model `HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestProductInvalid` must uses trait HalcyonLaravelBoilerplate\Vouchers\Traits\HasVoucherTrait.'
    );
