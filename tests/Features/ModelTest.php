<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\RequirementData;
use HalcyonLaravelBoilerplate\Vouchers\Models\Limit;
use HalcyonLaravelBoilerplate\Vouchers\Models\Voucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestOrderFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\VoucherFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use HalcyonLaravelBoilerplate\Vouchers\VoucherManager;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseHas;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;

it('replace under score on spaces', function () {
    VoucherFactory::new()->create(['code' => 'test word 123']);

    assertDatabaseHas(
        Voucher::class,
        [
            'code' => 'test_word_123',
        ]
    );
});

it('requirements data array', function () {
    $vh = prepareVoucherHelper();
    $vh->limit = 0;
    $vh->requirements = [
        new RequirementData(TestBasicVoucherRequirements::name(), $this->testProduct),
        new RequirementData(TestBasicVoucherRequirements::name(), 'test value'),
    ];
    $voucher = VoucherManager::create($vh);

    $data = $voucher->requirementsDataArray();

    assertCount(2, $data);

    assertEquals(TestBasicVoucherRequirements::label(), $data[0]['label']);
    assertEquals(TestBasicVoucherRequirements::name(), $data[0]['name']);
    assertEquals(null, $data[0]['value']);
    assertEquals($this->testProduct->{$this->testProduct->getRouteKeyName()}, $data[0]['model_url']);
    assertEquals($this->testProduct, $data[0]['model']);

    assertEquals(TestBasicVoucherRequirements::label(), $data[1]['label']);
    assertEquals(TestBasicVoucherRequirements::name(), $data[1]['name']);
    assertEquals('test value', $data[1]['value']);
    assertEquals(null, $data[1]['model_url']);
    assertEquals(null, $data[1]['model']);
});

it('used count single', function () {
    $voucher = VoucherManager::create(prepareVoucherHelper());

    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
    VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);

    assertEquals(4, $voucher->refresh()->getUsedCount());
    assertEquals(4, $voucher->refresh()->getUsedCount(true));
});

it('used count bulk', function () {
    // others
    $xVoucher = VoucherManager::create(prepareVoucherHelper(), null, 4);
    foreach ($xVoucher as $voucher) {
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
    }

    $vouchers = VoucherManager::create(prepareVoucherHelper(), null, 4);
    foreach ($vouchers as $voucher) {
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
    }

    assertDatabaseCount(Limit::class, 32);

    foreach ($vouchers as $voucher) {
        assertEquals(4, $voucher->refresh()->getUsedCount());
        assertEquals(16, $voucher->refresh()->getUsedCount(true));
    }
});

it('fetch fresh', function () {
    assertAllEmptyVoucherTable();
    VoucherManager::create(prepareVoucherHelper(), null, 10);
    assertDatabaseCount(Limit::class, 0);

    $voucher = Voucher::first();
    assertCount(10, $voucher->fetchBatch(Voucher::STATUS_ALL));
    assertCount(0, $voucher->fetchBatch(Voucher::STATUS_USED));
    assertCount(10, $voucher->fetchBatch(Voucher::STATUS_UNUSED));
});

it('fetch unused', function () {
    assertAllEmptyVoucherTable();
    VoucherManager::create(prepareVoucherHelper(), null, 10);

    foreach (Voucher::limit(5)->get() as $voucher) {
        VoucherManager::useCode($this->testUser, TestOrderFactory::new()->createOne(), $voucher->code);
    }

    assertDatabaseCount(Limit::class, 5);
    $voucher = Voucher::first();
    assertCount(10, $voucher->fetchBatch(Voucher::STATUS_ALL));
    assertCount(5, $voucher->fetchBatch(Voucher::STATUS_USED));
    assertCount(5, $voucher->fetchBatch(Voucher::STATUS_UNUSED));
});

it('fetch unused with used set false', function () {
    assertAllEmptyVoucherTable();
    VoucherManager::create(prepareVoucherHelper(), null, 10);

    foreach (Voucher::limit(5)->get() as $voucher) {
        VoucherManager::useCode($this->testUser, $this->testOrder, $voucher->code);
        VoucherManager::unUseCode($this->testUser, $this->testOrder, $voucher->code);
    }

    assertDatabaseCount(Limit::class, 5);

    $voucher = Voucher::first();
    assertCount(10, $voucher->fetchBatch(Voucher::STATUS_ALL));
    assertCount(0, $voucher->fetchBatch(Voucher::STATUS_USED));
    assertCount(10, $voucher->fetchBatch(Voucher::STATUS_UNUSED));
});
