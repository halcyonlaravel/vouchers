<?php

declare(strict_types=1);

use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\ComputationData;
use HalcyonLaravelBoilerplate\Vouchers\DataTransferObjects\VoucherData;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;

function prepareVoucherHelper(): VoucherData
{
    $vh = new VoucherData();
    $vh->limit = 0;
    $vh->limit_per_user = 0;
    $vh->requirements = null;
    $vh->computation = (new ComputationData(TestBasicBaseVoucherComputation::class))->setValue('test');
    $vh->valid_start_at = now();

    return $vh;
}

//    protected static function addKeyDataProvider(array $array): array
//    {
//        $return = [];
//
//        foreach ($array as $value) {
//            $keys = [];
//            foreach ($value as $i) {
//                if (is_bool($i)) {
//                    $keys[] = $i ? 'true' : 'false';
//                } else {
//                    $keys[] = $i;
//                }
//            }
//            $return[implode(', ', $keys)] = $value;
//        }
//
//        return $return;
//    }

function assertAllEmptyVoucherTable(): void
{
    foreach (config('vouchers.tables') as $key => $tableName) {
        if ($key == 'users') {
            continue;
        }
        test()->assertDatabaseCount($tableName, 0);
    }
}
