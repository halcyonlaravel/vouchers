<?php

declare(strict_types=1);

namespace HalcyonLaravelBoilerplate\Vouchers\Tests;

use HalcyonLaravelBoilerplate\Vouchers\Providers\VouchersServiceProvider;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestGuestFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestOrderFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestProductFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestProductInvalidFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Database\Factories\TestUserFactory;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Computation\TestCanMultipleBasicBaseVoucherComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\Override\TestComputation;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\Override\TestComputationModel;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\Override\TestIncompatible;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\Override\TestLimit;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\Override\TestRequirement;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\Override\TestVoucher;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestGuest;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestOrder;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestProduct;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestProductForFreeItem;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestProductInvalid;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Models\TestUser;
use HalcyonLaravelBoilerplate\Vouchers\Tests\Support\Helpers\Requirements\TestBasicVoucherRequirements;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Schema\Blueprint;
use Orchestra\Testbench\TestCase as Orchestra;

abstract class TestCase extends Orchestra
{
    public TestProduct $testProduct;

    protected TestUser $testUser;

    protected TestProductInvalid $testProductInvalid;

    protected TestGuest $testGuest;

    protected TestOrder $testOrder;

    protected function setUp(): void
    {
        Relation::requireMorphMap();
        Relation::morphMap([
            TestProduct::class,
            TestUser::class,
            TestOrder::class,
            TestProductInvalid::class,
            TestGuest::class,
        ]);

        parent::setUp();

        $this->setUpDatabase($this->app);

        $this->testUser = TestUser::first();
        $this->testProduct = TestProduct::first();
        $this->testProductInvalid = TestProductInvalid::first();
        $this->testGuest = TestGuest::first();
        $this->testOrder = TestOrder::first();
        //        dd(config('vouchers'));
    }

    public function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set(
            'database.connections.sqlite',
            [
                'driver' => 'sqlite',
                'database' => ':memory:',
                'prefix' => '',
            ]
        );
        $app['config']->set('vouchers.models.user', TestUser::class);
        $app['config']->set('vouchers.tables.users', (new TestUser())->getTable());
        $app['config']->set(
            'vouchers.mapping.computations',
            [
                TestBasicBaseVoucherComputation::class,
                TestCanMultipleBasicBaseVoucherComputation::class,
            ]
        );
        $app['config']->set(
            'vouchers.mapping.requirements',
            [
                TestBasicVoucherRequirements::class,
            ]
        );

        foreach (
            [
                'vouchers.models.computation' => TestComputation::class,
                'vouchers.models.computation_model' => TestComputationModel::class,
                'vouchers.models.incompatible' => TestIncompatible::class,
                'vouchers.models.limit' => TestLimit::class,
                'vouchers.models.requirement' => TestRequirement::class,
                'vouchers.models.voucher' => TestVoucher::class,
            ] as $key => $value
        ) {
            $app['config']->set($key, $value);
        }
    }

    protected function getPackageProviders($app): array
    {
        return [
            VouchersServiceProvider::class,
        ];
    }

    protected function setUpDatabase($app)
    {
        $schema = $app['db']->connection()->getSchemaBuilder();

        $schema->create(
            (new TestUser())->getTable(),
            function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->timestamps();
            }
        );

        $schema->create(
            (new TestProduct())->getTable(),
            function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->timestamps();
            }
        );

        $schema->create(
            (new TestProductInvalid())->getTable(),
            function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->timestamps();
            }
        );

        $schema->create(
            (new TestGuest())->getTable(),
            function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->timestamps();
            }
        );

        $schema->create(
            (new TestOrder())->getTable(),
            function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->timestamps();
            }
        );

        $schema->create(
            (new TestProductForFreeItem())->getTable(),
            function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->timestamps();
            }
        );

        $migration = require __DIR__.'/../database/migrations/create_vouchers_table.php.stub';
        $migration->up();

        TestUserFactory::new()->create();
        TestProductFactory::new()->create();
        TestProductInvalidFactory::new()->create();
        TestGuestFactory::new()->create();
        TestOrderFactory::new()->create();
    }
}
